var express = require('express'),
    app = express(),
    port = parseInt(process.env.SERVER_PORT, 10) || 8181,
    sslPort = 8182,
    compiler = require('./compiler'),
    fs = require('fs'),
    http = require('http'),
    https = require('https'),
    httpsOptions = {
      key: fs.readFileSync('./18089445-localhost_8182.key'),
      cert: fs.readFileSync('./18089445-localhost_8182.cert')
    };


app.use(express.static(__dirname + '/public'));
app.use('/local', compiler);

var server = http.createServer(app);
var secureServer = https.createServer(httpsOptions, app);

server.listen(port, function(){
  console.log('server listening at http://0.0.0.0:%s', port);
});

secureServer.listen(sslPort, function(){
  console.log('server listening at http://0.0.0.0:%s', sslPort);
});

// sslPort
//
// http.createServer(app).listen(port);
// secureServer.listen(sslPort);

//console.log('Static server listening at http://0.0.0.0:%s', port);
//app.listen(port);

var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var Promise = require('promise');
var config = require('./config.json');

var root = '/Users/martinegan/repos/amido-projects',
    files = {
        BagWeb: path.resolve(root, 'BagWeb/Source/Asos.Commerce.Bag.Web.Endpoint/client/build/bag.js'),
        BagWebCSS: path.resolve(root, 'BagWeb/Source/Asos.Commerce.Bag.Web.Endpoint/client/build/bag.css'),
        SavedItems: path.resolve(root, 'SavedItemsWeb/Source/Asos.Commerce.SavedItems.Web.Endpoint/client/build/savedItems.js'),
        SavedItemsCSS: path.resolve(root, 'SavedItemsWeb/Source/Asos.Commerce.SavedItems.Web.Endpoint/client/build/savedItems.css'),
        MiniBag: path.resolve(root, 'Minibag/client/build/miniBag.js'),
        MiniBagCSS: path.resolve(root, 'Minibag/client/build/miniBag.css'),
        BagSdk: path.resolve(root, 'BagSdk/build/bag-sdk.js'),
        SavedItemsSdk: path.resolve(root, 'SavedItemsSdk/build/saved-items-sdk.js'),
        MasterLayout: path.resolve(__dirname + '/public/MasterLayout.js'),
        MasterLayoutCSS: path.resolve('./file-replacement/public/masterlayout.css'),
        MasterLayoutCSSMobile: path.resolve(__dirname + '/public/masterlayoutmobile.css'),
        ResetCSS: path.resolve(__dirname + '/public/reset.css'),
        ResetCSSMobile: path.resolve(__dirname + '/public/resetmobile.css'),
        VoucherApp: path.resolve(__dirname + '/public/VoucherApp.js')
    };

function getFile(path){
    return new Promise(function(resolve, reject){
        fs.readFile(path, 'utf8', function(err, res){
            if(err){
                reject(err);
            }else{
                // if(config.useMockApis && path.indexOf('BagSdk') !== -1) {
                //     // Replace BAG API urls
                //     [
                //         'config.bagServiceUrl',
                //         'config.productCatalogueServiceUrl',
                //         'config.paymentOptionsServiceUrl',
                //         'config.subscriptionOptionsServiceUrl'
                //     ].forEach(function(url) {
                //         res = res.replace(url, "'http://localhost:1337/api'");
                //     });
                // }
                //
                // if(config.useMockApis && path.indexOf('SavedItemsSdk') !== -1) {
                //     // Replace Saved Items API urls
                //     [
                //         'config.savedItemsServiceUrl',
                //         'config.productCatalogueServiceUrl'
                //     ].forEach(function(url) {
                //         res = res.replace(url, "'http://localhost:1337/api'");
                //     });
                // }

                resolve(res);
            }
        })
    });
}

function compile(filePromises, res){
    Promise.all(filePromises).then(function(files){
        var ret = '/* COMPILED WITH LOCAL COMPILER MOCKED ON YOUR MACHINE */\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n';
        files.forEach(function(file){
            ret += file + '\r\n';
        });

        res.send(ret);
    }).catch(function(err){
        console.log(err);
        res.send(err);
    });
}

router.get('/masterlayout.js', function(req, res){
    res.header("Content-Type", "application/x-javascript; charset=utf-8");
    compile([
        getFile(files.BagSdk),
        getFile(files.SavedItemsSdk),
        getFile(files.MiniBag),
        getFile(files.MasterLayout)
    ], res);
});

router.get('/saveditems.js', function(req, res){
    res.header("Content-Type", "application/x-javascript; charset=utf-8");
    compile([
        getFile(files.SavedItems)
    ], res);
});

router.get('/saveditems.css', function(req, res){
    res.header("Content-Type", "text/css");
    compile([
        getFile(files.SavedItemsCSS)
    ], res);
});

router.get('/bag.js', function(req, res){
    res.header("Content-Type", "application/x-javascript; charset=utf-8");
    compile([
        getFile(files.BagWeb)
    ], res);
});

router.get('/bag.css', function(req, res){
    res.header("Content-Type", "text/css");
    compile([
        getFile(files.BagWebCSS)
    ], res);
});

router.get('/minibag.js', function(req, res){
    res.header("Content-Type", "application/x-javascript; charset=utf-8");
    compile([
        getFile(files.MiniBag)
    ], res);
});

router.get('/minibag.css', function(req, res){
    res.header("Content-Type", "text/css");
    compile([
        getFile(files.ResetCSS),
        getFile(files.MiniBagCSS),
        getFile(files.MasterLayoutCSS)
    ], res);
});

router.get('/minibag.mobile.css', function(req, res){
    res.header("Content-Type", "text/css");
    compile([
        getFile(files.ResetCSSMobile),
        getFile(files.MiniBagCSS),
        getFile(files.MasterLayoutCSSMobile)
    ], res);
});

router.get('/voucherApp.js', function(req, res){
    res.header("Content-Type", "application/x-javascript; charset=utf-8");
    compile([
        getFile(files.VoucherApp),
        getFile(files.BagSdk)
    ], res);
});


module.exports = router;


define('videoPlayerView',[],function () { return '<div class="asos-media-players">\r\n    <div class="amp-video-player amp-relative">\r\n        <video class="amp-page amp-video-element" data-bind="foreach: sources" controls="controls">\r\n            <source data-bind="attr: { type: type, src: src }" />\r\n        </video>\r\n        <div class="amp-page amp-page-message amp-error-page" data-bind="visible: isErrorPageVisible">\r\n            <div class="amp-message-container">\r\n                <div class="amp-message-box">\r\n                    <h2 data-bind="html: messages.errorHeaderText"></h2>\r\n                    <p data-bind="html: messages.errorMessageText"></p>\r\n                    <div class="amp-button amp-button-black" data-bind="html: messages.refreshButtonText, click: refreshButtonClicked"></div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="amp-page amp-page-message amp-unsupported-page" data-bind="visible: isUnsupportedPageVisible">\r\n            <div class="amp-message-container">\r\n                <div class="amp-message-box">\r\n                    <h2 data-bind="html: messages.unsupportedHeaderText"></h2>\r\n                    <p data-bind="html: messages.unsupportedMessageText"></p>\r\n                    <div class="amp-button amp-button-black" data-bind="html: messages.closeButtonText, click: closeButtonClicked"></div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>';});

define('utils/scene7.config',[], function () {
    'use strict';

    var Config = function (scene7imageServer, scene7videoServer) {
        this.jsonpCallback = 's7jsonResponse';
        this.jsonpErrorCallback = 's7jsonError';
        this.scene7imageServer = scene7imageServer;
        this.scene7videoServer = scene7videoServer;
        
        this.videoManifestFileUrlPattern = this.scene7videoServer + "/products/test-desc/[productID]-catwalk-AVS";
        this.videoM3u8FileUrlPattern = this.scene7videoServer + "/products/test-desc/[productID]-catwalk-AVS.m3u8";
        this.spinsetManifestFileUrlPattern = this.scene7imageServer + "/products/test-desc/[productID]-spinset";
        this.videoFileUrlPattern = this.scene7videoServer + "/products/[assetID]";
        this.imageUrlPattern = this.scene7imageServer + "/products/[assetID]";

        this.preferredBitrates = {
            "480": 800,
            "720": 1200,
            "1080": 2500
        };
        this.defaultFPS = 50;

        this.maxImageSize = {
            width: 4000,
            height: 4000
        };
        this.imageMinPartitionFactors = {
            horizontal: 3,
            vertical: 4
        };
    };

    return Config;
});
define('utils/scene7.DataProvider',[
    '$'
], 
    function ($) {
    'use strict';
    
    var DataProvider = function (scene7config) {
        this.scene7config = scene7config;
        this.manifestCache = {};
    };
    
    DataProvider.requests = [];
    
    DataProvider.prototype.getJson = function (url, onSuccess, onError) {
        if (!Object.prototype.hasOwnProperty.call(window, this.scene7config.jsonpErrorCallback)) {
            window[this.scene7config.jsonpErrorCallback] = function (data, requestId) {
                var request = DataProvider.requests[requestId];
                if (typeof request.onError === 'function') {
                    request.onError();
                }
            };
        }
        if (!Object.prototype.hasOwnProperty.call(window, this.scene7config.jsonpCallback)) {
            window[this.scene7config.jsonpCallback] = function (data, requestId) {
                var request = DataProvider.requests[requestId];
                if (typeof request.onSuccess === 'function') {
                    request.onSuccess(data);
                }
            };
        }
        
        DataProvider.requests.push({
            onSuccess: onSuccess,
            onError: onError
        });
        
        $.ajax({
            url: url.replace("[requestID]", DataProvider.requests.length - 1),
            jsonp: "true",
            dataType: "jsonp",
            success: function (response) {
                onSuccess(response);
            },
            error: onError
        });
    };
    
    DataProvider.prototype.getManifestData = function (baseUrl, onSuccess, onError) {
        function returnDataClone(data) {
            var dataClone = {};
            $.extend(true, dataClone, data);
            if (typeof onSuccess === 'function') {
                onSuccess(dataClone);
            }
        }
        if (this.manifestCache[baseUrl]) {
            setTimeout(
                function () {
                    returnDataClone(this.manifestCache[baseUrl]);
                }.bind(this),
                0
            );
        }
        else {
            var manifestUrl = baseUrl + ((baseUrl.indexOf('?') === -1) ? '?' : '&') + 'req=set,json&id=[requestID]';
            this.getJson(
                manifestUrl,
                function (imageData) {
                    this.manifestCache[baseUrl] = imageData;
                    returnDataClone(imageData);
                }.bind(this),
                onError
            );
        }
    };
    
    return DataProvider;
});
define('videoPlayer/DataProvider',[],function () {
    'use strict';

    var DataProvider = function (scene7config, scene7dataProvider) {
        this.scene7config = scene7config;
        this.scene7dataProvider = scene7dataProvider;
    };

    DataProvider.prototype.getAvailableVideoAssetsForProduct = function (
        productID,
        onSuccess,
        onError
    ) {
        var videoManifestFileUrl = this.scene7config
            .videoManifestFileUrlPattern.replace('[productID]', productID);
                    
        this.scene7dataProvider.getManifestData(videoManifestFileUrl, onSuccess, onError);
    };

    return DataProvider;
});
define('utils/deviceHelper',[],function () {
    'use strict';

    var DeviceHelper = function () {
        return this;
    };
    
    DeviceHelper.prototype.getScreenProperties = function (window) {
        return {
            width: window.screen.width,
            height: window.screen.height
        };
    };

    DeviceHelper.prototype.isPhysicalSizeGreaterThanLogicalSize = function (screen) {
        return screen &&
                screen.systemXDPI !== undefined &&
                screen.logicalXDPI !== undefined &&
                screen.systemXDPI > screen.logicalXDPI;
    };

    DeviceHelper.prototype.getDevicePixelRatio = function (window) {
        var ratio = 1;
        // To account for zoom, change to use deviceXDPI instead of systemXDPI
        if (this.isPhysicalSizeGreaterThanLogicalSize(window.screen)) {
            // Only allow for values > 1
            ratio = window.screen.systemXDPI / window.screen.logicalXDPI;
        } else if (window.devicePixelRatio !== undefined) {
            ratio = window.devicePixelRatio;
        }
        
        return ratio;
    };

    DeviceHelper.prototype.getWidthInPhysicalPixels = function (window) {
        return this.getScreenProperties(window).width * this.getDevicePixelRatio(window);
    };

    DeviceHelper.prototype.getHeightInPhysicalPixels = function (window) {
        return this.getScreenProperties(window).height * this.getDevicePixelRatio(window);
    };
        
    return new DeviceHelper();
});
define('videoPlayer/AssetsProvider',[
        'utils/deviceHelper'
    ], 
    function (deviceHelper) {
    'use strict';

    var AssetsProvider = function (scene7config, dataProvider) {
        this.scene7config = scene7config;
        this.dataProvider = dataProvider;
    };

    AssetsProvider.prototype.getAssetFromServerItem = function (serverItem) {
        return {
            bitrate: parseInt(serverItem.v.bitrate, 10),
            width: parseInt(serverItem.v.dx, 10),
            height: parseInt(serverItem.v.dy, 10),
            src: this.scene7config.videoFileUrlPattern.replace('[assetID]', serverItem.v.path),
            format: serverItem.v.suffix,
            type: this.getVideoTypeFromFormat(serverItem.v.suffix),
            fps: serverItem.userdata && serverItem.userdata[0] && serverItem.userdata[0].Video_Frame_Rate ? parseFloat(serverItem.userdata[0].Video_Frame_Rate) : this.scene7config.defaultFPS
        };
    };

    AssetsProvider.prototype.getAssetsByFormat = function (assets, format) {
        return assets.filter(function (asset) { return asset.format === format; });
    };

    AssetsProvider.prototype.getVideoTypeFromFormat = function (format) {
        switch (format) {
        case 'm3u8':
            return 'application/x-mpegurl';
        case 'mp4':
            return 'video/mp4';
        case 'ogg':
            return 'video/ogg';
        }
    };

    AssetsProvider.prototype.isBetterWidthDifference = function (temp_diff, diff) {
        return temp_diff === 0 ||
            (temp_diff > 0 && diff < 0) ||
            (Math.abs(temp_diff) <= Math.abs(diff) && temp_diff / diff > 0);
    };

    AssetsProvider.prototype.getBestAsset = function (assets, preferredBitrates, preferredFPS) {
        if (!assets || !assets.length) {
            return;
        }

        preferredFPS = preferredFPS || this.scene7config.defaultFPS;

        if (!preferredBitrates || !Object.keys(preferredBitrates).length) {
            preferredBitrates = this.scene7config.preferredBitrates;
        }
        var widthInPhysicalPixels = deviceHelper.getWidthInPhysicalPixels(window),
            diff = widthInPhysicalPixels,
            temp_diff,
            bestWidth,
            bestBitrateConfigOption,
            bestAsset,
            preferredBitrateOptions,
            preferredBitrate,
            assetsForWidth,
            bestAvailableBitrate,
            assetsForBitrate,
            i;
        // determine best width for device
        for (i = 0; i < assets.length; i++) {
            temp_diff = assets[i].width - widthInPhysicalPixels;

            if (!bestWidth || this.isBetterWidthDifference(temp_diff, diff)) {
                bestWidth = assets[i].width;
                diff = temp_diff;
            }
        }
        // determine correct bitrate configuration value
        diff = widthInPhysicalPixels;
        preferredBitrateOptions = Object
            .keys(preferredBitrates)
            .map(function (bitrateString) { return parseInt(bitrateString, 10); });
        for (i = 0; i < preferredBitrateOptions.length; i++) {
            temp_diff = preferredBitrateOptions[i] - widthInPhysicalPixels;
            if (!bestBitrateConfigOption || (Math.abs(temp_diff) <= Math.abs(diff))) {
                bestBitrateConfigOption = preferredBitrateOptions[i];
                diff = temp_diff;
            }
        }

        preferredBitrate = preferredBitrates[bestBitrateConfigOption];
        assetsForWidth = assets
            .filter(function (asset) { return asset.width === bestWidth; });
        // determine best video bitrate
        diff = preferredBitrate * 1000;
        for (i = 0; i < assetsForWidth.length; i++) {
            temp_diff = assetsForWidth[i].bitrate - (preferredBitrate * 1000);

            if (!bestAvailableBitrate || (Math.abs(temp_diff) <= Math.abs(diff))) {
                bestAvailableBitrate = assetsForWidth[i].bitrate;
                diff = temp_diff;
            }
        }

        assetsForBitrate = assetsForWidth
            .filter(function (asset) { return asset.bitrate === bestAvailableBitrate; });
        // determine best asset
        for (i = 0; i < assetsForBitrate.length; i++) {
            temp_diff = assetsForBitrate[i].fps - preferredFPS;

            if (!bestAsset || (Math.abs(temp_diff) <= Math.abs(diff))) {
                bestAsset = assetsForBitrate[i];
                diff = temp_diff;
            }
        }
        
        return bestAsset;
    };

    AssetsProvider.prototype.getAssets = function (
        productID,
        preferences,
        onSuccess,
        onError
    ) {
        var self = this;
        this.dataProvider.getAvailableVideoAssetsForProduct(productID, function (videoSet) {
            try {
                var assets = videoSet.set.item.map(self.getAssetFromServerItem.bind(self)),
                    bestAssets = [],
                    bestAssetForFormat,
                    i;
                assets.unshift({
                    bitrate: 0,
                    width: 0,
                    height: 0,
                    src: self.scene7config.videoM3u8FileUrlPattern.replace('[productID]', productID),
                    format: 'm3u8',
                    type: self.getVideoTypeFromFormat('m3u8')
                });
                for (i = 0; i < preferences.formats.length; i++) {
                    bestAssetForFormat = self
                    .getBestAsset(
                        self.getAssetsByFormat(assets, preferences.formats[i]),
                        preferences.preferredBitrates,
                        preferences.preferredFPS
                    );
                    if (bestAssetForFormat) {
                        bestAssets.push(bestAssetForFormat);
                    }
                }
                onSuccess(bestAssets);
            }
            catch (e) {
                if (typeof onError === "function") {
                    onError();
                }
            }
        }, onError);
    };

    return AssetsProvider;
});
define('videoPlayer/ViewModel',['ko'], function (ko) {
    function ViewModel(events, messages) {
        this.events = events;
        this.sources = ko.observableArray();
        this.messages = messages;
        this.isErrorPageVisible = ko.observable(false);
        this.isUnsupportedPageVisible = ko.observable(false);
        this.refreshButtonClicked = function () {
            this.events.trigger('refreshButtonClicked');
        }.bind(this);
        this.closeButtonClicked = function () {
            this.events.trigger('closeButtonClicked');
        }.bind(this);
    }
    
    return ViewModel;
});
define('utils/fullScreenHelper',["$"], function ($) {
    'use strict';
    
    var FullScreenHelper = function (events, document) {
        this.events = events;
        this.fullscreenLauncherName = this.getFullscreenLauncherName(document);
        this.fullscreenElementName = this.getFullscreenElementName(document);
        this.fullscreenCancelName = this.getFullscreenCancelName(document);
        this.elementStyle = {
            position: null,
            top: null,
            left: null,
            width: null,
            height: null,
            backgroundColor: null,
            zIndex: null
        };
        this.setEventListeners(document);
    };
    
    FullScreenHelper.prototype.getFullscreenLauncherName = function (document) {
        if (document.body.requestFullscreen) {
            return 'requestFullscreen';
        } else if (document.body.mozRequestFullScreen) {
            return 'mozRequestFullScreen';
        } else if (document.body.webkitRequestFullscreen) {
            return 'webkitRequestFullscreen';
        } else if (document.body.msRequestFullscreen) {
            return 'msRequestFullscreen';
        }
        return null;
    };
    
    FullScreenHelper.prototype.getFullscreenCancelName = function (document) {
        if (document.exitFullscreen) {
            return 'exitFullscreen';
        } else if (document.mozCancelFullScreen) {
            return 'mozCancelFullScreen';
        } else if (document.webkitExitFullscreen) {
            return 'webkitExitFullscreen';
        }
        return null;
    };
    
    FullScreenHelper.prototype.getFullscreenElementName = function (document) {
        if ('fullscreenElement' in document) {
            return 'fullscreenElement';
        } else if ('webkitFullscreenElement' in document) {
            return 'webkitFullscreenElement';
        } else if ('msFullscreenElement' in document) {
            return 'msFullscreenElement';
        } else if ('mozFullScreenElement' in document) {
            return 'mozFullScreenElement';
        }
        return null;
    };

    FullScreenHelper.prototype.getFullScreenElement = function (document) {
        if (this.fullscreenElement) {
            return this.fullscreenElement;
        } else if (this.fullscreenElementName) {
            return document[this.fullscreenElementName];
        }
        return;
    };

    FullScreenHelper.prototype.launchIntoFullscreen = function (document, element, useNative) {
        if (this.getFullScreenElement(document)) {
            return;
        }

        if (this.fullscreenLauncherName && useNative) {
            element[this.fullscreenLauncherName]();
        }
        else {
            this.elementStyle.position = element.style.position || '';
            this.elementStyle.top = element.style.top || '';
            this.elementStyle.left = element.style.left || '';
            this.elementStyle.width = element.style.width || '';
            this.elementStyle.height = element.style.height || '';
            this.elementStyle.backgroundColor = element.style.backgroundColor || '';
            this.elementStyle.zIndex = element.style.zIndex || '';
            
            element.style.position = 'fixed';
            element.style.top = '0px';
            element.style.left = '0px';
            element.style.width = '100%';
            element.style.height = '100%';
            element.style.backgroundColor = 'white';
            element.style.zIndex = '9999999';
            this.fullscreenElement = element;

            this.onFullScreenChange(document);
        }
    };

    FullScreenHelper.prototype.exitFullScreen = function (document, element, useNative) {
        if (element && this.getFullScreenElement(document) && this.getFullScreenElement(document) !== element) {
            return;
        }

        if (this.fullscreenCancelName && useNative) {
            document[this.fullscreenCancelName]();
        } else if (this.fullscreenElement) {
            this.fullscreenElement.style.position = this.elementStyle.position;
            this.fullscreenElement.style.top = this.elementStyle.top;
            this.fullscreenElement.style.left = this.elementStyle.left;
            this.fullscreenElement.style.width = this.elementStyle.width;
            this.fullscreenElement.style.height = this.elementStyle.height;
            this.fullscreenElement.style.backgroundColor = this.elementStyle.backgroundColor;
            this.fullscreenElement.style.zIndex = this.elementStyle.zIndex;
            this.fullscreenElement = null;
            this.elementStyle.position = null;
            this.elementStyle.top = null;
            this.elementStyle.left = null;
            this.elementStyle.width = null;
            this.elementStyle.height = null;
            this.elementStyle.backgroundColor = null;
            this.elementStyle.zIndex = null;
            this.onFullScreenChange(document);
        }
        else {
            try {
                var parent = element.parentNode;
                var nextSibling = element.nextSibling;
                parent.removeChild(element);
                parent.insertBefore(element, nextSibling);
            }
            catch (e) {
                // This try/catch is a workaround for Safari, 
                // which hangs if videoelement is removed from dom in fullscreenend event handler
            }
        }
    };
    
    FullScreenHelper.prototype.onFullScreenChange = function (document) {
        if (!this.getFullScreenElement(document)) {
            this.events.trigger('fullScreenExited');
        }
        else {
            this.events.trigger('fullScreenEntered');
        }
    };

    FullScreenHelper.prototype.setEventListeners = function (document) {
        $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', this.onFullScreenChange.bind(this, document));
    };

    return new FullScreenHelper($({}), document);
});
define(
    'videoPlayer/Controller',[
        '$',
        'utils/fullScreenHelper'        
    ], function ($, fullScreenHelper) {
        'use strict';
        
        var Controller = function (assetsProvider, params, viewModel, events, videoElement) {
            this.loadedAssets = null;
            this.loadedSources = undefined;
            this.progressEvents = {
                '0': false,
                '25': false,
                '50': false,
                '75': false,
                '100': false
            };
            this.assetsProvider = assetsProvider;
            this.events = events;
            this.viewModel = viewModel;
            this.videoElement = videoElement;
            this.params = {};
            this.isError = false;
            $.extend(true, this.params, params);
            this.setEventListeners();
        };
        
        Controller.prototype.setEventListeners = function () { 
            var videoElement = $(this.videoElement);
            videoElement.on('timeupdate', function () {
                this.onVideoTimeUpdated();
                if (this.videoElement.ended) {
                    this.onVideoPlaybackEnded();
                }
            }.bind(this));
            videoElement.on('webkitendfullscreen', function () {
                this.onFullScreenExited();
            }.bind(this));
            videoElement.on('error', function (e) {
                var originalElement = e.target;
                if (originalElement === this.videoElement) {
                    this.onVideoError();
                }
                else if (originalElement.tagName === "SOURCE") {
                    var sourceIndex = Array.prototype.indexOf.call(this.videoElement.querySelectorAll("source"), originalElement);
                    this.onVideoSourceError(sourceIndex);
                }
            }.bind(this));
            videoElement.on('canplay', function () {
                this.isError = false;
            }.bind(this));
            this.viewModel.events.on('videoSourceError', function (event) {
                this.onVideoSourceError(event.detail.sourceIndex);
            }.bind(this));
            this.viewModel.events.on('refreshButtonClicked', function () {
                this.onRefreshButtonClicked();
            }.bind(this));
            this.viewModel.events.on('closeButtonClicked', function () {
                this.onCloseButtonClicked();
            }.bind(this));
            fullScreenHelper.events.on('fullScreenExited', function () {
                this.onFullScreenExited();
            }.bind(this));
            fullScreenHelper.events.on('fullScreenEntered fullScreenExited', function () {
                this.onFullScreenChanged();
            }.bind(this));
        };

        Controller.prototype.onVideoPlaybackEnded = function () {
            if (--this.remainingLoopCount > 0) {
                this.videoElement.currentTime = 0;
                this.videoElement.play();
                this.events.trigger('loopEnded');
            } else {
                this.stop();
                this.events.trigger('allLoopsEnded');
            }
        };
        
        Controller.prototype.onVideoTimeUpdated = function () {
            var percentage = this.videoElement.currentTime * 100 / this.videoElement.duration;
            for (var value in this.progressEvents) {
                var intValue = parseInt(value, 10);
                if (!this.progressEvents[value] && percentage >= intValue) {
                    this.progressEvents[value] = true;
                    this.events.trigger('progress', { detail : { src: this.videoElement.currentSrc, value : intValue, isFullScreen: fullScreenHelper.getFullScreenElement(document) === this.videoElement } });
                }
            }
        };
        
        Controller.prototype.onVideoError = function () {
            this.isError = true;
            this.viewModel.isErrorPageVisible(true);
            this.events.trigger('playbackError');
        };
        
        Controller.prototype.onVideoSourceError = function (sourceIndex) {
            if (sourceIndex === this.viewModel.sources().length - 1) {
                this.onVideoError();
            }
        };
        
        Controller.prototype.onRefreshButtonClicked = function () {
            this.videoElement.load();
            this.start();
        };
        
        Controller.prototype.onCloseButtonClicked = function () {
            this.events.trigger('playerExitRequested');
        };
        
        Controller.prototype.onFullScreenExited = function () {
            if (this.params.closeOnFullScreenExited) {
                this.events.trigger('playerExitRequested');
            }
        };
        
        Controller.prototype.onFullScreenChanged = function () {
            if (!this.params.closeOnFullScreenExited && this.videoElement.readyState === 4) {
                var fakeProgressBarInteraction = function () {
                    if (this.videoElement.currentTime < 1) {
                        this.videoElement.currentTime += 1;
                        this.videoElement.currentTime -= 1;
                    }
                    else {
                        this.videoElement.currentTime -= 1;
                        this.videoElement.currentTime += 1;
                    }
                }.bind(this);
                
                if (!this.videoElement.paused) {
                    fakeProgressBarInteraction();
                    this.videoElement.pause();
                    this.videoElement.play();
                }
                else {
                    this.videoElement.play();
                    fakeProgressBarInteraction();
                    this.videoElement.pause();
                }
            }
        };

        Controller.prototype.hideInfoPages = function () {
            this.viewModel.isErrorPageVisible(false);
        };

        Controller.prototype.start = function () {
            this.hideInfoPages();
            this.resetProgressEvents();
            this.play(this.params.initialLoopCount || 1, this.params.startInFullScreen);
        };
        
        Controller.prototype.play = function (loopCount, startInFullScreen) {
            if (this.videoElement && this.videoElement.play && this.loadedAssets && !this.viewModel.isUnsupportedPageVisible()) {
                if (this.isError || JSON.stringify(this.loadedAssets) !== JSON.stringify(this.viewModel.sources())) {
                    this.viewModel.sources(this.loadedAssets);
                    this.videoElement.load();
                }
                this.remainingLoopCount = loopCount;
                if (this.videoElement.readyState > 0) {
                    this.videoElement.currentTime = 0;
                }
                if (startInFullScreen) {
                    fullScreenHelper.launchIntoFullscreen(document, this.videoElement, true);
                }
                this.videoElement.play();
            }
        };

        Controller.prototype.stop = function () {
            if (this.videoElement && this.videoElement.pause) {
                fullScreenHelper.exitFullScreen(document, this.videoElement, true);
                this.videoElement.pause();
                if (this.videoElement.readyState > 0) {
                    this.videoElement.currentTime = 0;
                }
            }
        };
        
        Controller.prototype.resetProgressEvents = function () {
            for (var value in this.progressEvents) {
                this.progressEvents[value] = false;
            }
        };
        
        Controller.prototype.hasSupportedAssets = function (assets) {
            var videoElement = document.createElement('video');
            if (!assets || !videoElement.canPlayType) {
                return false;
            }

            var supportedAssets = assets.filter(function (asset) {
                return videoElement.canPlayType(asset.type) !== '';
            });

            return supportedAssets.length > 0;
        };

        Controller.prototype.loadAssets = function (productID, config, onSuccess, onError) {
            this.loadedAssets = null;
            this.assetsProvider.getAssets(productID, config, function (assets) {
                if (assets && assets.length > 0) {
                    this.loadedAssets = assets;
                    this.viewModel.isUnsupportedPageVisible(!this.hasSupportedAssets(this.loadedAssets));
                    if (typeof onSuccess === 'function') {
                        onSuccess();
                    }
                } else {
                    if (typeof onError === 'function') {
                        onError();
                    }
                }
            }.bind(this), function () {
                if (typeof onError === 'function') {
                    onError();
                }
            });
        };
        
        return Controller;
    });
define('videoPlayer/component',[
    'ko', 
    '$', 
    'videoPlayerView',
    'utils/scene7.config',
    'utils/scene7.DataProvider',
    'videoPlayer/DataProvider',
    'videoPlayer/AssetsProvider',
    'videoPlayer/ViewModel',
    'videoPlayer/Controller'
], function (ko, $, viewHtmlString, Scene7config, Scene7DataProvider, DataProvider, AssetsProvider, ViewModel, Controller) {
    'use strict';
        
    return {
        viewModel: {
            createViewModel : function (params, componentInfo) {
                var viewModelEvents = $({});
                var controllerEvents = $({});
                var videoElement = componentInfo.element.querySelector('.asos-media-players .amp-video-player .amp-video-element');
                if (params.messages && params.messages.unsupportedHeaderText) {
                    params.messages.unsupportedHeaderText = params.messages.unsupportedHeaderText.replace('[BrowserName]', 'Your browser');
                }
                var viewModel = new ViewModel(viewModelEvents, params.messages);
                var scene7config = new Scene7config(params.scene7.imageServer, params.scene7.videoServer);
                var scene7dataProvider = new Scene7DataProvider(scene7config);
                var dataProvider = new DataProvider(scene7config, scene7dataProvider);
                var assetsProvider = new AssetsProvider(scene7config, dataProvider);
                var controller = new Controller(assetsProvider, params.player, viewModel, controllerEvents, videoElement);
                var api = {
                    start: controller.start.bind(controller),
                    stop: controller.stop.bind(controller),
                    loadAssets: controller.loadAssets.bind(controller),
                    events: controller.events
                };
                params.api(api);
                return viewModel;
            }
        },
        template: viewHtmlString,
        synchronous: true
    };
});

define('spinViewerView',[],function () { return '<div class="asos-media-players amp-relative">\r\n    <div class="amp-spin-viewer amp-relative" data-bind=\'css: { "spinnable": isSpinnable(), "pannable": isZoomOutButtonEnabled() }\'>\r\n        <div class="amp-spinner amp-relative" style="display: none" data-bind=\'component: { name: "asos-spinner", params: { scene7: params.scene7, viewer: { spinSpeed: params.viewer.spinSpeed, reverseVerticalDirection: params.viewer.reverseVerticalDirection, reverseHorizontalDirection: params.viewer.reverseHorizontalDirection, baseJpgQlt: params.viewer.baseJpgQlt, zoomJpgQlt: params.viewer.zoomJpgQlt, zoomFactors: params.viewer.zoomFactors.container, initialSize: params.viewer.initialSize, showHint: params.viewer.showContainerHint, preloadingTimeout: params.viewer.preloadingTimeout, handleLoadingErrors: params.viewer.handleLoadingErrors, useInitialAnimation: params.viewer.useInitialAnimation, animationIntervalStart: params.viewer.animationIntervalStart, animationIntervalPeak: params.viewer.animationIntervalPeak, animationFramesCount: params.viewer.animationFramesCount, animationEasing: params.viewer.animationEasing }, api: containerSpinViewerApi, messages: params.messages } }, visible: isContainerSpinViewerVisible\'></div>\r\n        <div class="amp-spinner amp-relative" style="display: none" data-bind=\'component: { name: "asos-spinner", params: { scene7: params.scene7, viewer: { spinSpeed: params.viewer.spinSpeed, reverseVerticalDirection: params.viewer.reverseVerticalDirection, reverseHorizontalDirection: params.viewer.reverseHorizontalDirection, baseJpgQlt: params.viewer.baseJpgQlt, zoomJpgQlt: params.viewer.zoomJpgQlt, zoomFactors: params.viewer.zoomFactors.fullScreen, forceFullScreen: true, showHint: false, preloadingTimeout: params.viewer.preloadingTimeout, handleLoadingErrors:params.viewer.handleLoadingErrors, useInitialAnimation: params.viewer.useInitialAnimation, animationIntervalStart: params.viewer.animationIntervalStart, animationIntervalPeak: params.viewer.animationIntervalPeak, animationFramesCount: params.viewer.animationFramesCount, animationEasing: params.viewer.animationEasing }, api: fullScreenSpinViewerApi, messages: params.messages } }, visible: isFullScreenSpinViewerVisible\'></div>\r\n        <div class="amp-controls" data-bind="visible: areControlsVisible()">\r\n            <div class="amp-button amp-wrapper" data-bind="visible: areZoomButtonsVisible, click: zoomInButtonClicked, css: { \'amp-disabled\': !isZoomInButtonEnabled() } ">\r\n                <div class="sprite plus"></div>\r\n            </div>\r\n            <div class="amp-button amp-wrapper" data-bind="visible: areZoomButtonsVisible, click: zoomOutButtonClicked, css: { \'amp-disabled\': !isZoomOutButtonEnabled() } ">\r\n                <div class="sprite minus"></div>\r\n            </div>\r\n            <div class="amp-button amp-wrapper" data-bind="visible: isFullScreenButtonVisible, click: fullScreenButtonClicked">\r\n                <div class="sprite expand" data-bind="css: { \'expand\': !isFullScreenSpinViewerVisible(), \'pinch\': isFullScreenSpinViewerVisible() }"></div>                \r\n            </div>\r\n            <div class="amp-button amp-wrapper" data-bind="visible: isCloseButtonVisible, click: closeButtonClicked">\r\n                <div class="sprite close"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>                ';});


define('spinnerView',[],function () { return '<div class="amp-spin-viewer amp-relative">\r\n    <div class="amp-page amp-spin">\r\n        <div class="amp-page amp-hint" data-bind="fadeVisible: isHintVisible">\r\n            <div class="amp-hint-container">\r\n                <div class="amp-hint-gesture amp-first">\r\n                    <div class="amp-hint-gesture-img sprite drag"></div>\r\n                </div>\r\n                <div class="amp-hint-gesture">\r\n                    <div class="amp-hint-gesture-img sprite tap"></div>\r\n                </div>\r\n                <div class="amp-clear"></div>\r\n                <div class="amp-hint-gesture amp-first">\r\n                    <span data-bind="text: messages.hintDragText"></span>\r\n                </div>\r\n                <div class="amp-hint-gesture">\r\n                    <span data-bind="text: messages.hintTapText"></span>\r\n                </div>\r\n                <div class="amp-clear"></div>                \r\n            </div>\r\n        </div>\r\n        <div class="amp-page amp-overlay"></div>\r\n        <div class="amp-page amp-images"></div>\r\n    </div>\r\n    <div class="amp-page amp-loading" data-bind="visible: isLoadingPageVisible">\r\n        <div class="amp-progress-container">\r\n            <div class="amp-loading-img"></div>\r\n            <span data-bind="text: loadingPercentage() + \'%\'"></span>\r\n        </div>\r\n    </div>\r\n    <div class="amp-page amp-page-message amp-error-page" data-bind="visible: isErrorPageVisible">\r\n        <div class="amp-message-container">\r\n            <div class="amp-message-box">\r\n                <h2 data-bind="html: messages.errorHeaderText"></h2>\r\n                <p data-bind="html: messages.spinsetErrorMessageText"></p>\r\n                <div class="amp-button amp-button-black" data-bind="html: messages.refreshButtonText, click: refreshButtonClicked"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>';});

define('utils/sizeHelper',[],function () {
    'use strict';

    function SizeHelper() {
    }

    SizeHelper.prototype.getProportionalSize = function (originalSize, modelSize) {
        var proportionalSize = {
                width: originalSize.width,
                height: originalSize.height
            },
            modelImageAspectRatio = modelSize.width / modelSize.height,
            originalSizeAspectRatio = originalSize.width / originalSize.height;
        
        if (originalSizeAspectRatio > modelImageAspectRatio) {
            proportionalSize.width = proportionalSize.height * modelImageAspectRatio;
        }
        else if (originalSizeAspectRatio < modelImageAspectRatio) {
            proportionalSize.height = proportionalSize.width / modelImageAspectRatio;
        }
        
        proportionalSize.width = Math.round(proportionalSize.width);
        proportionalSize.height = Math.round(proportionalSize.height);

        return proportionalSize;
    };
    
    SizeHelper.prototype.resizeCanvas = function (canvas, newSize) {
        var tempCanvas = document.createElement('canvas');
        tempCanvas.width = newSize.width;
        tempCanvas.height = newSize.height;
        tempCanvas.getContext('2d').drawImage(
            canvas,
            0,
            0,
            newSize.width,
            newSize.height
        );
        canvas.width = newSize.width;
        canvas.height = newSize.height;
        canvas.getContext('2d').drawImage(
            tempCanvas,
            0,
            0
        );
    };

    return new SizeHelper();
});
define('utils/imageProvider',['$'], function ($) {
    'use strict';
    
    var ImageProvider = function () {
    };
    
    ImageProvider.prototype.setImage = function (url, imageNode, onComplete, onError, useCors, forceReload) {
        
        if (useCors && $.support.cors) {
            imageNode.onload = onComplete;
            return $.ajax(
                url, 
                {
                    success: function (data) {
                        imageNode.src = url;
                    },
                    error: onError,
                    cache: !forceReload
                }
            );
        }
        else {
            if (forceReload) {
                url += '&' + (new Date()).getTime();
            }
            else if (imageNode.complete && imageNode.src === url) {
                if (typeof onComplete === 'function') {
                    onComplete();
                }
                return null;
            }
            imageNode.onload = onComplete;
            imageNode.onerror = onError;
            imageNode.src = url;
            return null;
        }
    };
    
    return new ImageProvider();
});
define('utils/scene7.ImageLoader',[
    '$',
    'utils/sizeHelper', 
    "utils/imageProvider"
    ], 
    function ($, sizeHelper, imageProvider) {
    'use strict'; 
    
    var ImageLoader = function (scene7Config, useCors) {
        this.urlQueryStringPattern = "wid=[width]&" + 
                "hei=[height]&" + 
                "size=[width],[height]&" +
                "rect=[startH],[startW],[lengthH],[lengthW]&" +
                "qlt=[qlt]";
        this.scene7Config = scene7Config;
        this.canUseCanvas = document.createElement('canvas').getContext;
        this.useCors = useCors;
        this.loadRequests = [];
    };
    
    ImageLoader.prototype.getPartitionFactors = function (imageSize, forcePartitioning) {
        return {
            horizontal: Math.max(Math.ceil(imageSize.width / this.scene7Config.maxImageSize.width), forcePartitioning ? this.scene7Config.imageMinPartitionFactors.horizontal : 1),
            vertical: Math.max(Math.ceil(imageSize.height / this.scene7Config.maxImageSize.height), forcePartitioning ? this.scene7Config.imageMinPartitionFactors.vertical : 1)
        };
    };      
    
    ImageLoader.prototype.getTile = function (loadRequestId, baseUrl, targetSize, hpIndex, vpIndex, partitionFactors, jpgQlt, onComplete, onError) {
        var image = new Image(),
            targetUrl = this.getTileUrl(baseUrl, targetSize, hpIndex, vpIndex, partitionFactors, jpgQlt),
            xhr = imageProvider.setImage(
                targetUrl, 
                image, 
                function () {
                    image.onload = null;
                    if (!this.loadRequests[loadRequestId].isActive) {
                        return;
                    }
                    if (typeof onComplete === 'function') {
                        onComplete(image);
                    }
                }.bind(this),
                function () {
                    image.onload = null;
                    if (!this.loadRequests[loadRequestId].isActive) {
                        return;
                    }
                    if (typeof onError === 'function') {
                        onError();
                    }
                }.bind(this),
                this.useCors
            );
        
        return {
            xhr: xhr,
            image: image
        };
    };    
    
    ImageLoader.prototype.addTile = function (loadRequestId, canvas, baseUrl, targetSize, outputSize, hpIndex, vpIndex, partitionFactors, jpgQlt, onComplete, onError) {
        if (!this.loadRequests[loadRequestId].isActive) {
            return;
        }
        
        this.loadRequests[loadRequestId].tiles.push(this.getTile(
            loadRequestId,
            baseUrl,
            targetSize,
            hpIndex,
            vpIndex,
            partitionFactors,
            jpgQlt,
            function (tile) {                
                if (this.loadRequests[loadRequestId].isActive) {
                    var tileBox = this.getTileBox(outputSize, hpIndex, vpIndex, partitionFactors);                    
                    canvas.getContext('2d').drawImage(
                        tile, 
                        tileBox.left, 
                        tileBox.top,
                        tileBox.width,
                        tileBox.height
                    );

                    if (typeof onComplete === 'function') {
                        onComplete();
                    }
                }
            }.bind(this),
            onError
        ));
    };      
    
    ImageLoader.prototype.addTiles = function (tilesData, loadRequestId, canvas, baseUrl, targetSize, outputSize, partitionFactors, jpgQlt, onComplete, onError) {
        var tilesToLoadCount = tilesData.length,
            onAddTileComplete = function () {
                tilesToLoadCount--;
                if (tilesToLoadCount === 0 && typeof onComplete === 'function') {
                    onComplete();
                }
            };
        if (tilesToLoadCount === 0 && typeof onComplete === 'function') {
            onComplete();
        }
        for (var i = 0; i < tilesData.length; i++) {
            this.addTile(
                loadRequestId,
                canvas,                
                baseUrl,
                targetSize,
                outputSize,
                tilesData[i].horizontal,
                tilesData[i].vertical,
                partitionFactors,
                jpgQlt,
                onAddTileComplete,
                onError
            );
        }
    };     

    ImageLoader.prototype.loadFullImage = function (loadRequestId, baseUrl, container, targetSize, outputSize, jpgQlt, imagePosition, onComplete, onError) {
        if (targetSize.width > this.scene7Config.maxImageSize.width || targetSize.height > this.scene7Config.maxImageSize.height) {
            targetSize = sizeHelper.getProportionalSize(this.scene7Config.maxImageSize, targetSize);
        }
        var image = container.querySelector('img'),
            forceReload = false;
        if (!image) {
            image = new Image();
            image.className = 'img';
        }
        else {
            forceReload = !image.complete;
        }
        image.style.width = Math.floor(outputSize.width) + 'px';
        image.style.height = Math.floor(outputSize.height) + 'px';
        if (imagePosition) {
            image.style.position = imagePosition.position;
            image.style.top = imagePosition.top;
            image.style.left = imagePosition.left;
        }
        image.setAttribute('data-url', baseUrl);
        image.setAttribute('data-target-qlt', jpgQlt);
        image.setAttribute('data-target-width', Math.floor(targetSize.width));
        image.setAttribute('data-target-height', Math.floor(targetSize.height));
        image.setAttribute('data-output-width', Math.floor(outputSize.width));
        image.setAttribute('data-output-height', Math.floor(outputSize.height));
                
        // Resize existing canvas
        var existingCanvas = container.querySelector('canvas');
        if (existingCanvas) {
            sizeHelper.resizeCanvas(existingCanvas, outputSize);
        }
        else {
            container.appendChild(image);
        }

        var targetUrl = this.getTileUrl(baseUrl, targetSize, 0, 0, { vertical: 1, horizontal: 1 }, jpgQlt),
            xhr = imageProvider.setImage(
                targetUrl, 
                image, 
                function () {
                    image.onload = null;
                    if (!this.loadRequests[loadRequestId].isActive) {
                        return;
                    }
                    if (existingCanvas) {
                        container.appendChild(image);
                        container.removeChild(existingCanvas);
                    }
                    if (typeof onComplete === 'function') {
                        onComplete();
                    }
                }.bind(this),
                function () {
                    image.onload = null;
                    if (!this.loadRequests[loadRequestId].isActive) {
                        return;
                    }
                    if (typeof onError === 'function') {
                        onError();
                    }
                }.bind(this),
                this.useCors,
                forceReload
            );

        this.loadRequests[loadRequestId].tiles.push({ image: image, xhr: xhr });
    };

    ImageLoader.prototype.loadImageToContainer = function (baseUrl, container, targetSize, outputSize, onComplete, onError, startingPoint, visibleArea, forcePartitioning, jpgQlt, imagePosition) {
        startingPoint = startingPoint || { x: targetSize.width / 2, y: targetSize.height / 2 };
        jpgQlt = jpgQlt || '';
        
        var partitionFactors = this.getPartitionFactors(targetSize, forcePartitioning),
            tilesToLoadCount = partitionFactors.horizontal * partitionFactors.vertical,
            fullImageArea = { topLeft: { x: 0, y: 0 }, bottomRight: { x: outputSize.width - 1, y: outputSize.height - 1 } },            
            startingPointTile = this.getTileDataForPoint(startingPoint, outputSize, partitionFactors),
            visibleAreaTiles,
            invisibleAreaTiles,
            loadRequestId = this.loadRequests.push({ isActive: true, tiles: [] }) - 1,
            targetCanvas,
            targetImage,
            fnAddTiles = function () {
                this.addTiles(
                    [startingPointTile],
                    loadRequestId,
                    targetCanvas,
                    baseUrl,
                    targetSize,
                    outputSize,
                    partitionFactors,
                    jpgQlt,
                    function () {
                        if (!this.loadRequests[loadRequestId].isActive) {
                            return false;
                        }
                        
                        this.addTiles(
                            visibleAreaTiles,
                            loadRequestId,
                            targetCanvas,
                            baseUrl,
                            targetSize,
                            outputSize,
                            partitionFactors,
                            jpgQlt,
                            function () {
                                if (!this.loadRequests[loadRequestId].isActive) {
                                    return false;
                                }
                                this.addTiles(
                                    invisibleAreaTiles,
                                    loadRequestId,
                                    targetCanvas,
                                    baseUrl,
                                    targetSize,
                                    outputSize,
                                    partitionFactors,
                                    jpgQlt,
                                    function () {
                                        this.loadRequests[loadRequestId].isActive = false;
                                        if (typeof onComplete === 'function') {
                                            onComplete();
                                        }
                                    }.bind(this),
                                    function () {
                                        if (!this.loadRequests[loadRequestId].isActive) {
                                            return false;
                                        }
                                        if (typeof onError === 'function') {
                                            onError();
                                        }
                                    }.bind(this)
                                );
                            }.bind(this),
                            function () {
                                if (!this.loadRequests[loadRequestId].isActive) {
                                    return false;
                                }
                                if (typeof onError === 'function') {
                                    onError();
                                }
                            }.bind(this)
                        );
                    }.bind(this),
                    function () {
                        if (!this.loadRequests[loadRequestId].isActive) {
                            return false;
                        }
                        if (typeof onError === 'function') {
                            onError();
                        }
                    }.bind(this)
                );
            }.bind(this)
        
        visibleArea = visibleArea || fullImageArea;
        
        visibleArea.topLeft.x = Math.max(Math.min(visibleArea.topLeft.x, fullImageArea.bottomRight.x), fullImageArea.topLeft.x);
        visibleArea.topLeft.y = Math.max(Math.min(visibleArea.topLeft.y, fullImageArea.bottomRight.y), fullImageArea.topLeft.y);
        visibleArea.bottomRight.x = Math.max(Math.min(visibleArea.bottomRight.x, fullImageArea.bottomRight.x), visibleArea.topLeft.x);
        visibleArea.bottomRight.y = Math.max(Math.min(visibleArea.bottomRight.y, fullImageArea.bottomRight.y), visibleArea.topLeft.y);
        visibleAreaTiles = this.getTilesDataForArea(visibleArea, outputSize, [startingPointTile], startingPointTile, partitionFactors);
        invisibleAreaTiles = this.getTilesDataForArea(fullImageArea, outputSize, [startingPointTile].concat(visibleAreaTiles), startingPointTile, partitionFactors);
        if (!this.canUseCanvas || (partitionFactors.vertical === 1 && partitionFactors.horizontal === 1)) {
            this.loadFullImage(loadRequestId, baseUrl, container, targetSize, outputSize, jpgQlt, imagePosition, onComplete, onError);
        }
        else {
            // Set up canvas
            targetCanvas = container.querySelector('canvas');
            if (targetCanvas) {
                sizeHelper.resizeCanvas(targetCanvas, outputSize);
                fnAddTiles();
            }
            else {
                targetCanvas = document.createElement('canvas');
                targetCanvas.className = 'img';
                targetCanvas.width = outputSize.width;
                targetCanvas.height = outputSize.height;
                
                // Draw background image on canvas
                var existingImage = container.querySelector('img');
                if (existingImage) {
                    var image = new Image();
                    image.onload = function () {
                        targetCanvas.getContext('2d').drawImage(
                            existingImage,
                            0,
                            0,
                            outputSize.width,
                            outputSize.height
                        );
                        targetCanvas.style.position = existingImage.style.position;
                        targetCanvas.style.top = existingImage.style.top;
                        targetCanvas.style.left = existingImage.style.left;
                        container.appendChild(targetCanvas);
                        $(existingImage).remove();
                        fnAddTiles();
                    }
                    image.src = existingImage.src;
                }
                else {
                    container.appendChild(targetCanvas);
                    fnAddTiles();
                }
            }
            
            targetCanvas.setAttribute('data-url', baseUrl);
            targetCanvas.setAttribute('data-target-qlt', jpgQlt);
            targetCanvas.setAttribute('data-target-width', Math.floor(targetSize.width));
            targetCanvas.setAttribute('data-target-height', Math.floor(targetSize.height));
            targetCanvas.setAttribute('data-output-width', Math.floor(outputSize.width));
            targetCanvas.setAttribute('data-output-height', Math.floor(outputSize.height));
        }
        
        return loadRequestId;
    };
    
    ImageLoader.prototype.getTilesDataForArea = function (area, outputSize, excludedTiles, centerTile, partitionFactors) {
        excludedTiles = excludedTiles || [];
        centerTile = centerTile || { horizontal: 0, vertical: 0 };

        var topLeftTile = this.getTileDataForPoint(area.topLeft, outputSize, partitionFactors),
            bottomRightTile = this.getTileDataForPoint(area.bottomRight, outputSize, partitionFactors),
            tiles = [],
            fnTileFilter = function (excludedTile) { return excludedTile.horizontal === hpIndex && excludedTile.vertical === vpIndex; };
        
        for (var hpIndex = topLeftTile.horizontal; hpIndex <= bottomRightTile.horizontal; hpIndex++) {
            for (var vpIndex = topLeftTile.vertical; vpIndex <= bottomRightTile.vertical; vpIndex++) {
                if (excludedTiles.filter(fnTileFilter).length === 0) {
                    tiles.push({
                        horizontal: hpIndex,
                        vertical: vpIndex
                    });
                }
            }
        }
        
        tiles.sort(function (tileA, tileB) {
            var distanceA = Math.sqrt(Math.pow(tileA.horizontal - centerTile.horizontal, 2) + Math.pow(tileA.vertical - centerTile.vertical)),
                distanceB = Math.sqrt(Math.pow(tileB.horizontal - centerTile.horizontal, 2) + Math.pow(tileB.vertical - centerTile.vertical));
            
            return distanceA > distanceB ? 1 : -1;

        });

        return tiles;
    };    

    ImageLoader.prototype.getTileDataForPoint = function (point, outputSize, partitionFactors) {
        var tile = {
                horizontal: null,
                vertical: null
            },
            tileBox;
        
        for (var hpIndex = 0; hpIndex < partitionFactors.horizontal && tile.horizontal === null; hpIndex++) {
            tileBox = this.getTileBox(outputSize, hpIndex, 0, partitionFactors);
            if (point.x >= tileBox.left &&
                    point.x < tileBox.left + tileBox.width) {
                tile.horizontal = hpIndex;
            }
        }
        for (var vpIndex = 0; vpIndex < partitionFactors.vertical && tile.vertical === null; vpIndex++) {
            tileBox = this.getTileBox(outputSize, 0, vpIndex, partitionFactors);
            if (point.y >= tileBox.top &&
                    point.y < tileBox.top + tileBox.height) {
                tile.vertical = vpIndex;
            }
        }
        
        return tile;
    };      
    
    ImageLoader.prototype.getTileBox = function (size, hpIndex, vpIndex, partitionFactors) {
        var left = this.getPartPosition(hpIndex, size.width, partitionFactors.horizontal),
            top = this.getPartPosition(vpIndex, size.height, partitionFactors.vertical),
            nextPartLeft = this.getPartPosition(hpIndex + 1, size.width, partitionFactors.horizontal),
            nextPartTop = this.getPartPosition(vpIndex + 1, size.height, partitionFactors.vertical);
        
        return {
            left: left,
            top: top,
            width: nextPartLeft - left,
            height: nextPartTop - top
        };
    };      
    
    ImageLoader.prototype.getPartPosition = function (index, length, partitionFactor) {
        return Math.floor(length * index / partitionFactor);
    };      
    
    ImageLoader.prototype.getTileUrl = function (baseUrl, targetSize, hpIndex, vpIndex, partitionFactors, jpgQlt) {
        var tileBox = this.getTileBox(targetSize, hpIndex, vpIndex, partitionFactors),
            urlConnector = baseUrl.indexOf("?") > -1 ? "&" : "?",
            targetUrlPattern = baseUrl + urlConnector + this.urlQueryStringPattern,
            targetUrl = targetUrlPattern
                .replace(/\[width\]/g, Math.floor(targetSize.width))
                .replace(/\[height\]/g, Math.floor(targetSize.height))
                .replace('[startH]', Math.floor(tileBox.left))
                .replace('[startW]', Math.floor(tileBox.top))
                .replace('[lengthH]', Math.floor(tileBox.width))
                .replace('[lengthW]', Math.floor(tileBox.height))
                .replace('[qlt]', jpgQlt);
        
        return targetUrl;
    };            
    
    ImageLoader.prototype.cancelAllLoadRequests = function () {
        for (var loadRequestId = 0; loadRequestId < this.loadRequests.length; loadRequestId++) {
            this.cancelLoadRequest(loadRequestId);
        }
    };

    ImageLoader.prototype.cancelLoadRequest = function (loadRequestId) {
        if (typeof loadRequestId === 'number' && loadRequestId < this.loadRequests.length && this.loadRequests[loadRequestId].isActive) {
            var loadRequest = this.loadRequests[loadRequestId];
           
            loadRequest.isActive = false;
            
            if (loadRequest.tiles.length) {
                var targetUrl = loadRequest.tiles[0].image.src,
                    tile = loadRequest.tiles.pop();

                while (tile) {
                    if (tile.xhr) {
                        tile.xhr.abort();
                    }
                    else {
                        tile.image.onload = null;
                        tile.image.src = targetUrl;
                    }                    
                    tile = loadRequest.tiles.pop();
                }
            }
        }
    };    

    return ImageLoader;
});
define('spinViewer/DataProvider',[],function () {
    'use strict';
    
    var DataProvider = function (scene7config, scene7dataProvider) {
        this.scene7config = scene7config;
        this.scene7dataProvider = scene7dataProvider;
    };
    
    DataProvider.prototype.mapManifestData = function (manifestData) {
        var spinsetData = {
            frameUrls: [],
            maxSize: {
                width: 0,
                height: 0
            }
        };
        
        if (manifestData.set.item.length > 0) {
            var rowsCount = manifestData.set.item[0].set ? manifestData.set.item.length : 1;
            if (rowsCount === 1) {
                spinsetData.frameUrls[0] = manifestData.set.item.map(function (frameData) { return this.scene7config.imageUrlPattern.replace('[assetID]', frameData.i.n); }.bind(this));
                spinsetData.maxSize.width = parseInt(manifestData.set.item[0].dx, 10);
                spinsetData.maxSize.height = parseInt(manifestData.set.item[0].dy, 10);
            }
            else {
                if (manifestData.set.item[0].set.item.length > 0) {
                    spinsetData.maxSize.width = parseInt(manifestData.set.item[0].set.item[0].dx, 10);
                    spinsetData.maxSize.height = parseInt(manifestData.set.item[0].set.item[0].dy, 10);
                }
                for (var r = 0; r < rowsCount; r++) {
                    spinsetData.frameUrls[r] = manifestData.set.item[r].set.item.map(function (frameData) { return this.scene7config.imageUrlPattern.replace('[assetID]', frameData.i.n); }.bind(this));
                }
            }
        }
        return spinsetData;
    };
    
    DataProvider.prototype.getSpinsetDataForImageUrls = function (imageUrls, onSuccess, onError) {
        var manifestUrl = "[baseUrl]?imageset={[imageSet]}&req=set,json",
            imageSet = [],
            imageUrl;
        for (var i = 0; i < imageUrls.length; i++) {
            imageUrl = imageUrls[i].split('?').shift();
            manifestUrl = manifestUrl.replace("[baseUrl]", imageUrl);
            imageSet.push(imageUrl.split('/').pop());
        }

        manifestUrl = manifestUrl.replace("[imageSet]", imageSet.join(','));
        
        this.scene7dataProvider.getManifestData(
            manifestUrl,
            function (manifestData) {
                var mappedDataArray = [];
                if (!Array.isArray(manifestData.set.item)) {
                    manifestData.set.item = [manifestData.set.item];
                }
                for (i = 0; i < manifestData.set.item.length; i++) {
                    mappedDataArray.push(this.mapManifestData({ "set": { "item": [manifestData.set.item[i]] } }));
                }
                if (typeof onSuccess === 'function') {                    
                    onSuccess(mappedDataArray);
                }
            }.bind(this),
            onError
        );
    };    

    DataProvider.prototype.getSpinsetDataForImageUrl = function (imageUrl, onSuccess, onError) {
        imageUrl = imageUrl.split('?').shift();
        var manifestUrl = imageUrl + '?req=set,json';

        this.scene7dataProvider.getManifestData(
            manifestUrl,
            function (manifestData) {
                if (typeof onSuccess === 'function') {
                    manifestData.set.item = [manifestData.set.item];
                    onSuccess(this.mapManifestData(manifestData));
                }
            }.bind(this),
            onError
        );
    };

    DataProvider.prototype.getSpinsetDataForProduct = function (productID, onSuccess, onError) {
        var spinsetManifestFileUrl = this.scene7config
            .spinsetManifestFileUrlPattern.replace('[productID]', productID);

        this.scene7dataProvider.getManifestData(
            spinsetManifestFileUrl,
            function (manifestData) {
                if (typeof onSuccess === 'function') {
                    onSuccess(this.mapManifestData(manifestData));
                }                
            }.bind(this),
            onError
        );
    };
    
    return DataProvider;
});
define('spinViewer/AssetsProvider',[], 
    function () {
    'use strict';
    
    var AssetsProvider = function (dataProvider) {
        this.dataProvider = dataProvider;
    };
    
    AssetsProvider.prototype.getAssets = function (
        id,
        onSuccess,
        onError
    ) {
        var self = this;
        if (Array.isArray(id)) {
            this.dataProvider.getSpinsetDataForImageUrls(id, onSuccess, onError);
        }
        else {
            if (!isNaN(id)) {
                this.dataProvider.getSpinsetDataForProduct(id, onSuccess, onError);
            }
            else {
                this.dataProvider.getSpinsetDataForImageUrl(id, onSuccess, onError);
            }
        }
    };
    
    return AssetsProvider;
});
define('spinViewer/SpinnerViewModel',['ko'], function (ko) {
    function ViewModel(events, messages) {
        this.events = events;
        this.messages = messages;
        this.isLoadingPageVisible = ko.observable(false);
        this.loadingPageMessage = ko.observable();
        this.loadingPercentage = ko.observable(0);
        this.isErrorPageVisible = ko.observable(false);
        this.isFullScreen = ko.observable(false);
        this.isHintVisible = ko.observable(false);
        this.refreshButtonClicked = function () {
            this.events.trigger('refreshButtonClicked');
        }.bind(this);  
    }
    
    return ViewModel;
});
define(
    'utils/ImageScaler',["$"], 
    function ($) {
        'use strict';
        
        var ImageScaler = function (options) {
            this.options = {};
            $.extend(true, this.options, options);
        };
                
        ImageScaler.prototype.getScaledImageSize = function (zoomFactor) {
            var scaledWidth = this.getScaledImageLength(
                this.options.baseSize.width,
                    this.options.maxSize.width,
                    zoomFactor),
                scaledHeight = this.getScaledImageLength(
                    this.options.baseSize.height,
                    this.options.maxSize.height,
                    zoomFactor);
            
            return {
                width: scaledWidth,
                height: scaledHeight
            };
        };
        
        /* private stuff follows */
        ImageScaler.prototype.getScaledImageLength = function (baseLength, maxLength, zoomFactor) {
            if (!zoomFactor) {
                return baseLength;
            }
            
            // We want the scaler to be based on the difference between the maximum size and the base image
            // in such a way that a 0.5 split means, that the total image area ratio between 1st level and base level
            // is the same as the ratio between max level and first level, while the 0.33 split means 
            // that ratio between 1st level and base level is a square root of the ratio between max level and 1st level
            //
            // E.g. Level 0 image: 100 x 100 (10K area)
            //      Level 1 (0.5 factor) image: x * y (z area)
            //      Level 2 (max) image: 1000 x 1000 (1M area)      
            //      z / 10K = 1M / z
            //
            // E.g 2 (more levels)
            //
            // Level 0 image: 100 x 100 (10K area)
            // Level 1 (0.33 factor) image: a * b (c area)
            // Level 2 (0.66 factor) image: x * y (z area)
            // Level 3 (max) image: 1000 x 1000 (1M area)
            //      c / 10K = z / c = 1M /z
            //
            // It gives a nice visual experience, where evenly-distributed factors (like [0.5], [0.33,0.66] or [0.25,0.5,0.75])
            // result in an image that seems to be scaled by a constant factor (even though it isn't), making the zoom look natural
            //
            // And now OBEY MY MATH that puts it into a one-liner
            return Math.pow((Math.pow(baseLength, 1 / zoomFactor) * maxLength / baseLength), zoomFactor);
        };
        
        return ImageScaler;
    });
define(
    'utils/ImageZoomer',["ko", "$", 'utils/sizeHelper'], 
    function (ko, $, sizeHelper) {
        'use strict';
        
        var ImageZoomer = function (imageLoader, inputImageScaler, outputImageScaler, options) {
            this.events = $({});
            this.imageLoader = imageLoader;
            this.inputImageScaler = inputImageScaler;
            this.outputImageScaler = outputImageScaler;
            this.currentLevel = ko.observable(0);
            this.options = {};
            $.extend(true, this.options, options);
            this.options.zoomFactors = [0].concat(this.options.zoomFactors);
            this.updateCanZoomDummy = ko.observable(false);
            this.canZoomIn = ko.computed(this.canZoomIn, this);
            this.canZoomOut = ko.computed(this.canZoomOut, this);
            this.currLoadRequestId = null;      
            this.currentPosition = {
                x: 0,
                y: 0
            };
            this.currentSize = {
                width: 0,
                height: 0
            };
        };
        
        ImageZoomer.prototype.init = function () {
            var container = $(this.options.container);
            this.containerSize = {
                width: this.options.containerSize ? this.options.containerSize.width : container.width(),
                height: this.options.containerSize ? this.options.containerSize.height : container.height()
            };
            this.currentSize = this.getOutputImageSize(this.currentLevel());
            this.currentPosition = this.getSanitizedPosition(
                this.currentPosition,
                this.currentSize
            );
            this.applyPosition(true);
            this.updateCanZoomDummy(!this.updateCanZoomDummy());
        };
        
        ImageZoomer.prototype.resize = function (reload, options, onComplete) {
            this.options.containerSize = options.containerSize;
            this.options.outputBaseSize = options.outputBaseSize;
            this.outputImageScaler.options.baseSize = options.imageScalersOptions.outputImageScalerOptions.baseSize;
            this.outputImageScaler.options.maxSize = options.imageScalersOptions.outputImageScalerOptions.maxSize;
            this.init();
            if (reload) {
                this.zoom(function () { return this.currentLevel(); }.bind(this), null, onComplete);
            }
        };

        ImageZoomer.prototype.canZoomIn = function () {
            this.updateCanZoomDummy();
            var minOutputImageSize = this.getOutputImageSize(0),
                maxOutputImagesize = this.getOutputImageSize(this.options.zoomFactors.length - 1);
            return this.currentLevel() < this.options.zoomFactors.length - 1 &&
                (minOutputImageSize.width < maxOutputImagesize.width ||
                minOutputImageSize.height < maxOutputImagesize.height);
        };
        
        ImageZoomer.prototype.canZoomOut = function () {
            this.updateCanZoomDummy();
            var minOutputImageSize = this.getOutputImageSize(0),
                currOutputImagesize = this.getOutputImageSize(this.currentLevel());
            return this.currentLevel() > 0 &&
                (minOutputImageSize.width < currOutputImagesize.width ||
                minOutputImageSize.height < currOutputImagesize.height);
        };        

        ImageZoomer.prototype.zoomIn = function (loop, focalPointInContainer, onComplete) {
            if (this.canZoomIn()) {
                this.zoom(
                    function () {
                        return this.currentLevel() + 1;
                    }.bind(this),
                    focalPointInContainer,
                    onComplete
                );
            }
            else {
                if (loop) {
                    this.zoomReset(onComplete);
                }
                else {
                    if (typeof onComplete === 'function') {
                        onComplete();
                    }
                }
            }
        };
        
        ImageZoomer.prototype.zoomOut = function (focalPointInContainer, onComplete) {
            if (this.canZoomOut()) {
                this.zoom(
                    function () {
                        return this.currentLevel() - 1;
                    }.bind(this),
                    focalPointInContainer,
                    onComplete
                );
            }
            else {
                if (typeof onComplete === 'function') {
                    onComplete();
                }
            }
        };
        
        ImageZoomer.prototype.zoomReset = function (onComplete, skipReload) {
            if (skipReload) {
                var nextLevel = 0,
                    newOutputImageSize = this.getOutputImageSize(nextLevel),
                    newImageBox = this.getZoomedImageBox(this.currentSize, this.currentPosition, newOutputImageSize);
                this.currentSize = newImageBox.size;
                this.currentPosition = newImageBox.position;
                this.applyPosition(true);                
                this.currentLevel(nextLevel);
                if (typeof onComplete === 'function') {
                    setTimeout(onComplete, 0);
                }
            }
            else {
                this.zoom(
                    function () {
                        return 0;
                    }.bind(this),
                undefined,
                onComplete
                );
            }
        };

        
        ImageZoomer.prototype.loadImage = function (baseUrl, container, targetSize, outputSize, onComplete, onError, startingPoint, visibleArea, forcePartitioning, jpgQlt, imagePosition) {
            if (!baseUrl) {
                return;
            }
            this.imageLoader.cancelLoadRequest(this.currLoadRequestId);
            this.currLoadRequestId = this.imageLoader.loadImageToContainer(
                baseUrl, 
                container, 
                targetSize, 
                outputSize,
                function () {
                    this.currLoadRequestId = null;
                    if (typeof onComplete === 'function') {
                        setTimeout(onComplete, 0);
                    }
                }.bind(this),
                onError,
                startingPoint, 
                visibleArea, 
                forcePartitioning, 
                jpgQlt,
                imagePosition
            );
        };

        ImageZoomer.prototype.zoom = function (fnGetNextLevel, focalPointInContainer, onComplete, onError) {
            this.imageLoader.cancelLoadRequest(this.currLoadRequestId);
            var nextLevel = fnGetNextLevel(),
                newSourceImageSize = this.getInputImageSize(nextLevel),     
                newOutputImageSize = this.getOutputImageSize(nextLevel),
                newImageBox = this.getZoomedImageBox(this.currentSize, this.currentPosition, newOutputImageSize, focalPointInContainer);
            this.currentSize = newImageBox.size;
            this.currentPosition = newImageBox.position;
            this.applyPosition(true);
            this.loadImage(
                this.options.baseUrl, 
                this.options.container, 
                this.getInputImageSize(nextLevel), 
                this.getOutputImageSize(nextLevel), 
                function () {
                    this.applyPosition();
                    if (typeof onComplete === 'function') {
                        onComplete();
                    }
                }.bind(this),
                onError,
                newImageBox.focalPointOnSourceImage,
                this.getVisibleArea(newImageBox.position),
                nextLevel > 0,
                nextLevel > 0 ? this.options.zoomJpgQlt : this.options.baseJpgQlt,
                {
                    'position': 'absolute',
                    'top': Math.floor(this.currentPosition.y) + 'px',
                    'left': Math.floor(this.currentPosition.x) + 'px'
                }
            );

            this.currentLevel(nextLevel);
        };
        
        ImageZoomer.prototype.getVisibleArea = function (position) {
            return {
                topLeft: {
                    x: -position.x,
                    y: -position.y
                },
                bottomRight: {
                    x: this.containerSize.width - position.x,
                    y: this.containerSize.height - position.y
                }
            };
        };

        ImageZoomer.prototype.getZoomedImageBox = function (currentSize, currentPosition, newSize, focalPointInContainer) {
            if (!focalPointInContainer) {
                focalPointInContainer = {
                    x: this.containerSize.width / 2,
                    y: this.containerSize.height / 2
                };
            }

            focalPointInContainer = this.getSanitizedPointOnImage(focalPointInContainer, currentSize);
           
            var pointOnOutputImage = this.getPointOnOutputImageFromContainer(focalPointInContainer, currentSize);
            var sizeDiff = {
                width: newSize.width - currentSize.width,
                height: newSize.height - currentSize.height
            };
            var absoluteScale = newSize.width / currentSize.width;
            var pointOnNewSourceImage = {
                x: pointOnOutputImage.x * absoluteScale,
                y: pointOnOutputImage.y * absoluteScale
            };
            var focalPointPositionPercentage = {
                x: pointOnOutputImage.x / currentSize.width,
                y: pointOnOutputImage.y / currentSize.height
            };
            var positionShift = {
                x: sizeDiff.width * focalPointPositionPercentage.x,
                y: sizeDiff.height * focalPointPositionPercentage.y
            };
            var newPosition = this.getSanitizedPosition(
                {
                    x: currentPosition.x - positionShift.x,
                    y: currentPosition.y - positionShift.y
                },
                newSize
            );
            
            return {
                size: newSize,
                position: newPosition,
                focalPointOnSourceImage: pointOnNewSourceImage
            };
        };        

        ImageZoomer.prototype.moveBy = function (shift) {
            if (!this.canZoomOut() || !(shift.x || shift.y)) {
                return;
            }
            this.currentPosition = this.getSanitizedPosition(
                {
                    x: this.currentPosition.x + shift.x, 
                    y: this.currentPosition.y + shift.y
                },
                this.getOutputImageSize(this.currentLevel())
            );
            this.applyPosition();
        };

        ImageZoomer.prototype.applyPosition = function (resize) {
            var container = $(this.options.container),
                width = Math.floor(this.currentSize.width),
                height = Math.floor(this.currentSize.height),
                img = container.find('.img');

            img.css({
                'position': 'absolute',
                'top': Math.floor(this.currentPosition.y) + 'px',
                'left': Math.floor(this.currentPosition.x) + 'px'
            });
            img.attr({
                'data-output-width': width,
                'data-output-height': height
            });
            container.find('img').css({
                'width': width + 'px',
                'height': height + 'px',
            });
            var canvas = this.options.container.querySelector('canvas');
            if (canvas && resize) {
                sizeHelper.resizeCanvas(canvas, { width: width, height: height });
            }
        };
        
        ImageZoomer.prototype.getInputImageSize = function (level) {
            var zoomFactor = this.options.zoomFactors[level],
                scaledImageSize = this.inputImageScaler.getScaledImageSize(zoomFactor);
            return scaledImageSize.width < this.options.outputBaseSize.width || scaledImageSize.height < this.options.outputBaseSize.height ? this.options.outputBaseSize : scaledImageSize;
        };        

        ImageZoomer.prototype.getOutputImageSize = function (level) {
            if (level === 0) {
                return this.options.outputBaseSize;
            }
            else {
                var zoomFactor = this.options.zoomFactors[level],
                    scaledImageSize = this.outputImageScaler.getScaledImageSize(zoomFactor);
                
                return scaledImageSize.width < this.options.outputBaseSize.width || scaledImageSize.height < this.options.outputBaseSize.height ? this.options.outputBaseSize : scaledImageSize;
            }
        };
        
        ImageZoomer.prototype.setPlaceholderImage = function (image) {
            var nextLevel = 0,
                newSourceImageSize = this.getInputImageSize(nextLevel),     
                newOutputImageSize = this.getOutputImageSize(nextLevel),
                newImageBox = this.getZoomedImageBox(this.currentSize, this.currentPosition, newOutputImageSize);            
            this.currentSize = newImageBox.size;
            this.currentPosition = newImageBox.position;
            var elementsToRemove = $(this.options.container).children();
            this.options.container.appendChild(image);
            this.applyPosition(true);
            elementsToRemove.remove();
        };

        ImageZoomer.prototype.getImage = function () {
            var container = $(this.options.container),
                img = container.find('.img')[0];
            
            if (img.nodeName === 'CANVAS') {
                var image = new Image();
                image.src = img.getContext('2d').toDataURL();
                return image;
            }
            else {
                return img.cloneNode();
            }
        };

        ImageZoomer.prototype.getBezel = function (imageSize) {
            return {
                horizontal: (Math.max(this.containerSize.width - Math.floor(imageSize.width), 0) / 2),
                vertical: (Math.max(this.containerSize.height - Math.floor(imageSize.height), 0) / 2)
            };
        };        

        ImageZoomer.prototype.getSanitizedPosition = function (position, imageSize) {
            var bezel = this.getBezel(imageSize),
                sanitizedPosition = {
                    x: Math.floor(position.x),
                    y: Math.floor(position.y)
                };
            sanitizedPosition.x = Math.min(sanitizedPosition.x, bezel.horizontal);
            sanitizedPosition.y = Math.min(sanitizedPosition.y, bezel.vertical);
            sanitizedPosition.x = Math.max(sanitizedPosition.x, this.containerSize.width - Math.floor(imageSize.width) - bezel.horizontal);
            sanitizedPosition.y = Math.max(sanitizedPosition.y, this.containerSize.height - Math.floor(imageSize.height) - bezel.vertical);
            return sanitizedPosition;
        };
        
        ImageZoomer.prototype.getSanitizedPointOnImage = function (point, imageSize) {
            var bezel = this.getBezel(imageSize),
                sanitizedPoint = {
                    x: point.x,
                    y: point.y
                };
                        
            sanitizedPoint.x = Math.max(sanitizedPoint.x, bezel.horizontal);
            sanitizedPoint.y = Math.max(sanitizedPoint.y, bezel.vertical);
            sanitizedPoint.x = Math.min(sanitizedPoint.x, this.containerSize.width - bezel.horizontal);
            sanitizedPoint.y = Math.min(sanitizedPoint.y, this.containerSize.height - bezel.vertical);
            return sanitizedPoint;
        };        
        
        ImageZoomer.prototype.getPointOnOutputImageFromContainer = function (pointInContainer) {
            return {
                x: pointInContainer.x - this.currentPosition.x,
                y: pointInContainer.y - this.currentPosition.y
            };
        };        

        return ImageZoomer;
    });
define(
    'spinViewer/SpinnerController',[
        "ko",
        "$",
        "utils/deviceHelper",
        "utils/fullScreenHelper",
        'utils/sizeHelper',
        "utils/ImageScaler",
        "utils/ImageZoomer"
    ], function (ko, $, deviceHelper, fullScreenHelper, sizeHelper, ImageScaler, ImageZoomer) {
        'use strict';
        
        var Controller = function (assetsProvider, imageLoader, params, viewModel, containers, events) {
            this.assetsProvider = assetsProvider;
            this.imageLoader = imageLoader;
            this.events = events;
            this.containers = containers;
            this.params = {
                showHint: false,
                spinSpeed: 1,
                rowLoadDelay: 200,
                frameLoadDelay: 10,
                maxPixelRatio: 1.5,
                hintDisplayTime: 3000,
                preloadingTimeout: 0, // no timeout error
                handleLoadingErrors: true,
                framesCachingQueueInterval: 35,
                hiddenAnimationInterval: 25,
                useInitialAnimation: false,
                animationFramesCount: -1,
                animationIntervalStart: 40,
                animationIntervalPeak: 5,
                animationEasing: 'full-sinus'
            };
            this.loadedAssets = null;
            this.targeSize = null;
            this.frames = [[]];
            this.lastInitArguments = null;
            this.viewModel = viewModel;
            this.currentRowIndex = ko.observable(0);
            this.currentFrameIndex = ko.observable();
            this.isInitialized = ko.observable(false);
            $.extend(true, this.params, params);
            this.params.devicePixelRatio = Math.min(deviceHelper.getDevicePixelRatio(this.containers.window), this.params.maxPixelRatio);
            this.updateZoomerDummy = ko.observable(false);
            this.hasError = ko.observable(false);
            this.isLoading = ko.observable(false);
            this.getCurrentZoomer = ko.computed(this.getCurrentZoomer, this);
            this.canZoomIn = ko.computed(this.canZoomIn, this);
            this.canZoomOut = ko.computed(this.canZoomOut, this);
            this.setEventListeners();
            this.isInitInProgress = false;
        };
               
        Controller.prototype.animationIntervalEasing = function (step, duration) {
            var range = this.params.animationIntervalPeak - this.params.animationIntervalStart,
                factor;
            
            switch (this.params.animationEasing) {
                case 'half-sinus':
                    factor = Math.sin(Math.PI * step / duration);                    
                    break;
                default:                    
                    factor = (Math.sin((Math.PI * 2 * step / duration - Math.PI / 2)) + 1) / 2;
                    break;
            }

            return this.params.animationIntervalStart + factor * range;
        };

        Controller.prototype.refresh = function () {
            this.init.apply(this, this.lastInitArguments);
        };

        Controller.prototype.init = function (initFrame, fullScreenContainer) {
            if (this.isInitInProgress) {
                return;
            }
            this.isInitInProgress = true;
            this.lastInitArguments = arguments;

            initFrame = initFrame || {};

            var initRowIndex = initFrame.rowIndex || 0,
                initFrameIndex = initFrame.frameIndex || 0;

            this.containerSizes = {
                spin: {
                    width: $(this.containers.spin).width(),
                    height: $(this.containers.spin).height()
                }
            };
            if (!this.loadedAssets) {
                this.loadedAssets = {
                    frameUrls: [],
                    maxSize: {
                        width: initFrame.initImage.size.width,
                        height: initFrame.initImage.size.height,
                        isFake: true
                    }
                };
            }
            
            if (this.params.forceFullScreen) {
                this.viewModel.isFullScreen(true);
                this.containers.fullScreenContainer = fullScreenContainer;
                fullScreenHelper.launchIntoFullscreen(document, fullScreenContainer, false);
                fullScreenContainer = $(fullScreenContainer);
                this.containerSizes.spin = {
                    width: fullScreenContainer.width(),
                    height: fullScreenContainer.height()
                };

                this.targetSize = this.getFullScreenInitialTargetSize();
            }
            else {
                this.targetSize = this.getContainerInitialTargetSize();
            }

            if (initFrame && (initFrame.initZoom || initFrame.initImage)) {
                var imageZoomer = this.getZoomer(initRowIndex, initFrameIndex);
                imageZoomer.options.baseUrl = this.loadedAssets.maxSize.isFake ? '' : this.loadedAssets.frameUrls[initRowIndex][initFrameIndex];                

                if (initFrame.initZoom && initFrame.initZoom.zoomLevel) {
                    imageZoomer.setZoom(initFrame.initZoom);
                }
                else {
                    imageZoomer.zoomReset(null, true);
                }

                if (initFrame.initImage) {
                    initFrame.initImage.image.className = 'img';
                    imageZoomer.setPlaceholderImage(initFrame.initImage.image);
                }
                
                if (this.loadedAssets.frameUrls.length > 1) {
                    this.isInitialized(false);
                }
                else {
                    this.isInitialized(true);
                }
                this.showFrame(initRowIndex, initFrameIndex, true);
                this.isInitInProgress = false;
            }
            else {
                if (!this.isInitialized()) {
                    this.preloadImages(initRowIndex, initFrameIndex);
                }
                else {
                    this.resize(function () {
                        this.showFrame(initRowIndex, initFrameIndex, true);
                        this.isInitInProgress = false;
                    }.bind(this));
                }
            }
        };
        
        Controller.prototype.animateSpin = function (startFrame, stopFrame, easingFunction, onComplete) {
            var currentFrame = startFrame,
                step = 0,
                duration = stopFrame > startFrame ? (stopFrame - startFrame) : (this.frames[this.currentRowIndex()].length + stopFrame - startFrame),
                animate = function () {
                    this.showFrame(this.currentRowIndex(), currentFrame, true);
                    if (currentFrame === stopFrame) {
                        if (typeof onComplete === 'function') {
                            onComplete();
                        }
                    }
                    else {
                        currentFrame++;
                        step++;
                        if (currentFrame >= this.frames[this.currentRowIndex()].length) {
                            currentFrame = 0;
                        }
                        setTimeout(animate, easingFunction(step, duration));
                    }
                }.bind(this);

            animate();
        };

        Controller.prototype.getZoomer = function (rowIndex, frameIndex) {
            var imageZoomer,
                outputBaseSize = this.getOutputBaseSize(),
                imageScalersOptions = this.getImageScalersOptions(outputBaseSize);
            
            this.frames[rowIndex] = this.frames[rowIndex] || [];
            if (this.frames[rowIndex][frameIndex]) {
                imageZoomer = this.frames[rowIndex][frameIndex];
            }
            else {
                imageZoomer = this.createZoomer();
                this.frames[rowIndex][frameIndex] = imageZoomer;
                this.updateZoomerDummy(!this.updateZoomerDummy());
            }
            
            imageZoomer.inputImageScaler.options.baseSize = imageScalersOptions.inputImageScalerOptions.baseSize;
            imageZoomer.inputImageScaler.options.maxSize = imageScalersOptions.inputImageScalerOptions.maxSize;
                        
            imageZoomer.resize(
                false, 
                {
                    containerSize: this.containerSizes.spin,
                    outputBaseSize: outputBaseSize,
                    imageScalersOptions: imageScalersOptions
                }
            );            

            return imageZoomer;
        };

        Controller.prototype.getImageScalersOptions = function (outputBaseSize) {
            var inputImageScalerMaxSize = {
                    width: this.loadedAssets.maxSize.width,
                    height: this.loadedAssets.maxSize.height
                },
                inputImageScalerMinSize = {
                    width: Math.min(this.loadedAssets.maxSize.width, this.targetSize.width * this.params.devicePixelRatio),
                    height: Math.min(this.loadedAssets.maxSize.height, this.targetSize.height * this.params.devicePixelRatio)
                },
                outputImageScalerMinSize = outputBaseSize,
                outputImageScalerMaxSize = {
                    width: Math.max(outputBaseSize.width, this.loadedAssets.maxSize.width / this.params.devicePixelRatio),
                    height: Math.max(outputBaseSize.height, this.loadedAssets.maxSize.height / this.params.devicePixelRatio)
                };
            
            return {
                inputImageScalerOptions: { baseSize: inputImageScalerMinSize, maxSize: inputImageScalerMaxSize },
                outputImageScalerOptions: { baseSize: outputImageScalerMinSize, maxSize: outputImageScalerMaxSize }
            };
        };

        Controller.prototype.createZoomer = function (baseUrl) {
            var container = document.createElement("div"),
                outputBaseSize = this.getOutputBaseSize(),
                imageScalersOptions = this.getImageScalersOptions(outputBaseSize),
                inputImageScaler = new ImageScaler(imageScalersOptions.inputImageScalerOptions),
                outputImageScaler = new ImageScaler(imageScalersOptions.outputImageScalerOptions);
            container.className = "amp-frame";
            var imageZoomer = new ImageZoomer(
                this.imageLoader, 
                inputImageScaler,
                outputImageScaler,
                {
                    baseUrl: baseUrl, 
                    zoomFactors: this.params.zoomFactors, 
                    container: container,
                    containerSize: this.containerSizes.spin,
                    outputBaseSize: outputBaseSize,
                    devicePixelRatio: this.params.devicePixelRatio,
                    baseJpgQlt: this.params.baseJpgQlt,
                    zoomJpgQlt: this.params.zoomJpgQlt
                }
            );

            return imageZoomer;
        };

        Controller.prototype.getContainerInitialTargetSize = function () {
            var targetSize = sizeHelper.getProportionalSize(
                {
                    width: this.containerSizes.spin.width,
                    height: this.containerSizes.spin.height
                }, 
                this.loadedAssets.maxSize
            );
            if (this.params.initialSize) {
                targetSize.width = this.params.initialSize.width;
                targetSize.height = this.loadedAssets.maxSize.height * targetSize.width / this.loadedAssets.maxSize.width;
            }
            
            return targetSize;
        };
        
        Controller.prototype.getFullScreenInitialTargetSize = function () {
            var screenSize = deviceHelper.getScreenProperties(this.containers.window),
                targetSize = {
                    width: screenSize.width,
                    height: screenSize.height
                },               
                screenRatio = targetSize.width / targetSize.height,
                imageRatio = this.loadedAssets.maxSize.width / this.loadedAssets.maxSize.height;
            
            if ((screenRatio < 1 && imageRatio > 1) ||
                    (screenRatio > 1 && imageRatio < 1)) {
                var oldWidth = targetSize.width;
                targetSize.width = targetSize.height;
                targetSize.height = oldWidth;
            }
            return sizeHelper.getProportionalSize(targetSize, this.loadedAssets.maxSize);
        };
        
        Controller.prototype.getOutputBaseSize = function () {
            return sizeHelper.getProportionalSize(this.containerSizes.spin, this.loadedAssets.maxSize);
        };
        
        Controller.prototype.preloadImages = function (initRowIndex, initFrameIndex, onError) {
            this.viewModel.loadingPercentage(0);
            this.isInitialized(false);
            this.hasError(false);
            this.isLoading(true);            
            var totalImages,     
                preloadingTimeout = null,
                hasError = false,    
                framesCachingQueue = [],
                framesCachingInterval = null,
                fnCacheNextFrame = function (onAllFramesCached) {
                    var frameData = framesCachingQueue.pop();
                    if (frameData) {
                        this.showFrame(frameData.rowIndex, frameData.frameIndex, true);
                        imagesToCache--;
                        this.viewModel.loadingPercentage(Math.floor(100 * (totalImages - imagesToCache) / totalImages));
                    }
                    else {
                        if (!imagesToLoad) {
                            clearInterval(framesCachingInterval);
                            framesCachingInterval = null;
                            if (typeof onAllFramesCached === 'function') {
                                onAllFramesCached();
                            }
                        }
                    }
                },
                fnLoadError = function () {
                    hasError = true;
                    imagesToLoad--;
                    imagesToCache--;
                    if (!imagesToLoad) {
                        this.isLoading(false);
                        clearTimeout(preloadingTimeout);
                        preloadingTimeout = null;
                        clearInterval(framesCachingInterval);
                        framesCachingInterval = null;
                        this.hasError(true);
                        this.isInitInProgress = false;
                    }
                },
                fnOnAllImagesLoaded = function () {
                    this.isLoading(false);
                    clearTimeout(preloadingTimeout);
                    preloadingTimeout = null;
                    this.hasError(hasError);
                    if (!hasError) {
                        var framesCount = this.frames[initRowIndex].length,
                            animationStartFrame = initFrameIndex,
                            animationStopFrame = (framesCount + initFrameIndex - 1) % framesCount;
                            
                        if (this.params.showHint && (!window.sessionStorage || !window.sessionStorage.getItem('spinViewerHintDisplayed'))) {
                            this.viewModel.isHintVisible(true);
                            setTimeout(function () {
                                this.viewModel.isHintVisible(false);
                                if (window.sessionStorage) {
                                    window.sessionStorage.setItem('spinViewerHintDisplayed', true);
                                }
                            }.bind(this), this.params.hintDisplayTime);
                        }
                            
                        if (this.params.useInitialAnimation) {
                            animationStartFrame = (framesCount + initFrameIndex - this.params.animationFramesCount) % framesCount;
                            animationStopFrame = initFrameIndex;
                                
                            this.showFrame(initRowIndex, animationStartFrame, true);
                            this.viewModel.isLoadingPageVisible(false);
                            this.animateSpin(animationStartFrame, animationStopFrame, this.animationIntervalEasing.bind(this), function () {
                                this.isInitialized(true);
                                this.showFrame(initRowIndex, initFrameIndex, true);
                                this.isInitInProgress = false;
                            }.bind(this));
                        }
                        else {
                            this.isInitialized(true);
                            this.viewModel.isLoadingPageVisible(false);
                            this.showFrame(initRowIndex, initFrameIndex, true);
                            this.isInitInProgress = false;
                        }
                    }
                    else {
                        this.isInitInProgress = false;
                    }
                },
                fnLoadSuccess = function (rowIndex, frameIndex, imageZoomer) {
                    imageZoomer.applyPosition();
                    imagesToLoad--;
                    if (totalImages > 1) {
                        framesCachingQueue.push({ rowIndex: rowIndex, frameIndex: frameIndex })
                    }
                }, 
                fnLoadFrame = function (rowIndex, frameIndex, imageZoomer) {
                    imageZoomer.loadImage(
                        this.loadedAssets.frameUrls[rowIndex][frameIndex], 
                        imageZoomer.options.container, 
                        imageZoomer.inputImageScaler.options.baseSize, 
                        imageZoomer.options.outputBaseSize, 
                        fnLoadSuccess.bind(this, rowIndex, frameIndex, imageZoomer),
                        (this.params.handleLoadingErrors ? fnLoadError.bind(this) : fnLoadSuccess.bind(this, rowIndex, frameIndex, imageZoomer)),
                        null,
                        null,
                        totalImages === 1,
                        this.params.baseJpgQlt);
                };
            if (!this.loadedAssets || this.loadedAssets.maxSize.isFake) {
                return;
            }
            totalImages = this.loadedAssets.frameUrls.length * this.loadedAssets.frameUrls[0].length;
            var imagesToLoad = totalImages,
                imagesToCache = totalImages;
                
            if (totalImages > 1) {
                this.viewModel.isLoadingPageVisible(true);

            }
            
            framesCachingInterval = setInterval(fnCacheNextFrame.bind(this, fnOnAllImagesLoaded.bind(this)), this.params.framesCachingQueueInterval);

            this.imageLoader.cancelAllLoadRequests();
            if (this.params.handleLoadingErrors && this.params.preloadingTimeout) {
                preloadingTimeout = setTimeout(function () {
                    clearInterval(framesCachingInterval);
                    framesCachingInterval = null;
                    this.imageLoader.cancelAllLoadRequests();                    
                    this.isLoading(false);
                    this.hasError(true);
                    this.isInitInProgress = false;
                }.bind(this), this.params.preloadingTimeout);
            }

            for (var rowIndex = 0; rowIndex < this.loadedAssets.frameUrls.length; rowIndex++) {
                for (var frameIndex = 0; frameIndex < this.loadedAssets.frameUrls[rowIndex].length; frameIndex++) {
                    var imageZoomer = this.getZoomer(rowIndex, frameIndex);
                    imageZoomer.options.baseUrl = this.loadedAssets.frameUrls[rowIndex][frameIndex];
                    //setTimeout(fnLoadFrame.bind(this, rowIndex, frameIndex, imageZoomer), rowIndex * this.params.rowLoadDelay + frameIndex * this.params.frameLoadDelay);
                    fnLoadFrame.call(this, rowIndex, frameIndex, imageZoomer);
                }
            }
        };
        
        Controller.prototype.canZoomIn = function () {
            var zoomer = this.getCurrentZoomer();
            return this.loadedAssets && 
                this.loadedAssets.maxSize && 
                !this.loadedAssets.maxSize.isFake && 
                zoomer && 
                zoomer.canZoomIn();
        };
        
        Controller.prototype.canZoomOut = function () {
            var zoomer = this.getCurrentZoomer();
            return this.loadedAssets && 
                this.loadedAssets.maxSize && 
                !this.loadedAssets.maxSize.isFake && 
                zoomer && 
                zoomer.canZoomOut();
        };
        
        Controller.prototype.getCurrentZoomer = function () {
            this.updateZoomerDummy();
            if (this.currentRowIndex() < this.frames.length && 
                this.frames[this.currentRowIndex()] && 
                this.currentFrameIndex() < this.frames[this.currentRowIndex()].length) {
                return this.frames[this.currentRowIndex()][this.currentFrameIndex()];
            }
            return null;
        };
        
        Controller.prototype.getCurrentImage = function () {
            return getCurrentZoomer().getImage();
        };

        Controller.prototype.getCurrentFrame = function () {
            return {
                rowIndex: this.currentRowIndex(), 
                frameIndex: this.currentFrameIndex(), 
                zoomer: this.getCurrentZoomer()
            };
        };
        
        Controller.prototype.showFrame = function (rowIndex, frameIndex, force) {
            if ((force || this.isInitialized()) && (rowIndex != this.currentRowIndex() || frameIndex != this.currentFrameIndex())) {
                var elementsToRemove = $(this.containers.images).children();
                this.currentRowIndex(rowIndex);
                this.currentFrameIndex(frameIndex);
                this.updateZoomerDummy(!this.updateZoomerDummy());
                var frame = this.getCurrentFrame();
                this.containers.images.appendChild(frame.zoomer.options.container);
                elementsToRemove.remove();
                this.events.trigger('frameShown', frame);
            }
        };
        
        Controller.prototype.setEventListeners = function () {
            var isDrag = false,
                panStartRowIndex,
                panStartFrameIndex,
                panStartPosition,
                shift,
                $containerSpin = $(this.containers.spin),
                tapped = false,
                killEvent = function (e) {
                    var zoomer = this.getCurrentZoomer();
                    if (e.type === "mousemove" || zoomer.canZoomOut() || this.frames.length > 1 || this.frames[this.currentRowIndex()].length > 1) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                }.bind(this),
                dblClick = function (e) {
                    if (tapped) {
                        clearTimeout(tapped);
                        tapped = null;
                    }
                    isDrag = false;
                    this.events.trigger('dblclick', e);
                }.bind(this),
                mouseDown = function (e) {
                    if (this.isInitialized() && (!e.originalEvent.touches || e.originalEvent.touches.length <= 1)) {
                        isDrag = true;
                        panStartFrameIndex = this.currentFrameIndex();
                        panStartRowIndex = this.currentRowIndex();
                        var gesture = e.originalEvent.touches ? e.originalEvent.touches[0] : e.originalEvent;
                        shift = {
                            x: 0,
                            y: 0
                        };
                        panStartPosition = {
                            x: gesture.clientX,
                            y: gesture.clientY
                        };
                        if (!tapped) {
                            tapped = setTimeout(function () {
                                tapped = null;
                            }.bind(this), 500);
                        } else {
                            if (!e.originalEvent || e.originalEvent.type !== "mousedown") {
                                clearTimeout(tapped);
                                tapped = null;
                                isDrag = false;
                                this.events.trigger('dblclick', gesture);
                            }
                        }
                        killEvent(e);
                    }
                }.bind(this),
                mouseMove = function (e) {
                    if (this.isInitialized() && isDrag) {                        
                        var gesture = e.originalEvent.touches ? e.originalEvent.touches[0] : e.originalEvent;
                        var partialShift = {
                            x: gesture.clientX - panStartPosition.x - shift.x,
                            y: gesture.clientY - panStartPosition.y - shift.y
                        };
                        shift = {
                            x: gesture.clientX - panStartPosition.x,
                            y: gesture.clientY - panStartPosition.y
                        };
                        if (tapped && Math.sqrt(Math.pow(shift.x, 2) + Math.pow(shift.y, 2)) > 20) {
                            clearTimeout(tapped);
                            tapped = null;
                        }
                        var zoomer = this.getCurrentZoomer();
                        if (zoomer.canZoomOut()) {
                            zoomer.moveBy(partialShift);
                        }
                        else {
                            if (this.frames.length > 1 || this.frames[this.currentRowIndex()].length > 1) {
                                var targetRowIndex = this.getRemoteRowIndex(panStartRowIndex, this.getMatrixShift(this.params.reverseVerticalDirection, shift.y, this.frames.length, this.containerSizes.spin.height));
                                var targetFrameIndex = this.getRemoteFrameIndex(panStartFrameIndex, this.getMatrixShift(this.params.reverseHorizontalDirection, shift.x, this.frames[0].length, this.containerSizes.spin.width));
                                if (targetRowIndex !== this.currentRowIndex() || targetFrameIndex !== this.currentFrameIndex()) {
                                    this.events.trigger('spun');
                                    this.showFrame(targetRowIndex, targetFrameIndex);
                                }                                
                            }
                        }
                        killEvent(e);
                    }
                }.bind(this),
                mouseUp = function (e) {
                    if (this.isInitialized()) {
                        isDrag = e.originalEvent.touches && e.originalEvent.touches.length === 1;
                    }
                }.bind(this);
            
            if (window.navigator.PointerEnabled || window.PointerEvent || window.navigator.MSPointerEnabled || window.MSPointerEvent) {
                $containerSpin.on("pointerdown", mouseDown);
                $containerSpin.on("MSPointerDown", mouseDown);
                $(document).on("pointerup", mouseUp);
                $(document).on("MSPointerUp", mouseUp);
                $(document).on("pointermove", mouseMove);
                $(document).on("MSPointerMove", mouseMove);
            }
            else if ("ontouchstart" in document || (document.documentElement && "ontouchstart" in document.documentElement)) {
                $containerSpin.on("touchstart", mouseDown);
                $(document).on("touchend", mouseUp);
                $(document).on("touchmove", mouseMove);
            }
            else {
                $containerSpin.on("mousedown", mouseDown);
                $(document).on("mouseup", mouseUp);
                $(document).on("mousemove", mouseMove);
                $containerSpin.on("dblclick", dblClick);
            }

            this.hasError.subscribe(function (hasError) {
                this.viewModel.isErrorPageVisible(hasError);
            }.bind(this));

            this.viewModel.events.on('refreshButtonClicked', function () {
                this.events.trigger('refreshButtonClicked');
            }.bind(this));
        };
        
        Controller.prototype.getMatrixShift = function (isReverse, positionShift, totalCells, containerLength) {
            var shift = (isReverse ? -1 : 1) * this.params.spinSpeed * positionShift * totalCells / containerLength;
            
            return shift > 0 ? Math.floor(shift) : Math.ceil(shift);
        };
        
        Controller.prototype.getRemoteFrameIndex = function (startFrameIndex, frameShift) {
            var frameIndex = (startFrameIndex + frameShift) % this.frames[0].length;
            if (frameIndex < 0) {
                frameIndex = this.frames[0].length + frameIndex;
            }
            
            return frameIndex;
        };
        
        Controller.prototype.getRemoteRowIndex = function (startRowIndex, rowShift) {
            var rowIndex = (startRowIndex + rowShift) % this.frames.length;
            if (rowIndex < 0) {
                rowIndex = this.frames.length + rowIndex;
            }
            
            return rowIndex;
        };
        
        Controller.prototype.setAssets = function (assets) {
            this.isInitialized(false);
            this.loadedAssets = assets;
        };
        
        Controller.prototype.zoomIn = function (force, positionInContainer, onComplete) {
            var zoomer = this.getCurrentZoomer();
            if (zoomer && (force || zoomer.canZoomIn())) {
                this.imageLoader.cancelAllLoadRequests();
                zoomer.zoomIn(force, positionInContainer, onComplete);
            }
            else {
                if (typeof onComplete === 'function') {
                    onComplete();
                }
            }
        };
        
        Controller.prototype.zoomOut = function (focalPointInContainer, onComplete) {
            var zoomer = this.getCurrentZoomer();
            if (zoomer && zoomer.canZoomOut()) {
                this.imageLoader.cancelAllLoadRequests();
                zoomer.zoomOut(focalPointInContainer, onComplete);
            }
            else {
                if (typeof onComplete === 'function') {
                    onComplete();
                }
            }
        };
        
        Controller.prototype.zoomReset = function (onComplete) {
            var zoomer = this.getCurrentZoomer();
            if (zoomer) {
                this.imageLoader.cancelAllLoadRequests();
                zoomer.zoomReset(onComplete);
            }
            else {
                if (typeof onComplete === 'function') {
                    onComplete();
                }
            }
        };
        
        Controller.prototype.resize = function (onComplete) {
            var zoomer,
                zoomersToResize = this.frames.length * this.frames[0].length,
                onResizeComplete = function () {
                    zoomersToResize--;
                    if (!zoomersToResize) {
                        if (typeof onComplete === 'function') {
                            onComplete();
                        }
                    }
                };
            this.containerSizes = {
                spin: {
                    width: $(this.containers.spin).width(),
                    height: $(this.containers.spin).height()
                }
            };
            if (!zoomersToResize) {
                if (typeof onComplete === 'function') {
                    onComplete();
                }
                return;
            }
            for (var rowIndex = 0; rowIndex < this.frames.length; rowIndex++) {
                for (var frameIndex = 0; frameIndex < this.frames[rowIndex].length; frameIndex++) {
                    var outputBaseSize = this.getOutputBaseSize();
                    zoomer = this.frames[rowIndex][frameIndex];
                    zoomer.resize(
                        true,
                        {
                            containerSize: this.containerSizes.spin,
                            outputBaseSize: outputBaseSize,
                            imageScalersOptions: this.getImageScalersOptions(outputBaseSize)
                        },
                        onResizeComplete
                    );
                }
            }
        };
        
        Controller.prototype.loadAssets = function (id, onSuccess, onError) {
            this.loadedAssets = null;
            this.assetsProvider.getAssets(
                id,
                function (assets) {
                    if (assets.maxSize.width && assets.maxSize.height) {
                        this.loadedAssets = assets;
                        if (typeof onSuccess === "function") {
                            onSuccess(assets);
                        }
                    }
                    else {
                        if (typeof onError === "function") {
                            onError();
                        }
                    }
                }.bind(this), 
                onError
            );
        };
        
        return Controller;
    });
define('bindings/fadeVisible',['ko', '$'], function (ko, $) {
    'use strict';
    
    function FadeVisible() {
    }
    
    FadeVisible.prototype.init = function (element, valueAccessor) {
        var value = valueAccessor();
        $(element).toggle(ko.unwrap(value));
    };
    
    FadeVisible.prototype.update = function (element, valueAccessor) {
        var value = valueAccessor();
        if (ko.unwrap(value)) {
            $(element).fadeIn();
        }
        else {
            $(element).fadeOut();
        }
    };
    
    ko.bindingHandlers.fadeVisible = new FadeVisible();
    
    return ko.bindingHandlers.fadeVisible;
});
define('spinViewer/spinnerComponent',[
    '$',
    'spinnerView',
    'utils/scene7.config',
    'utils/scene7.ImageLoader',
    'utils/scene7.DataProvider',
    'spinViewer/DataProvider',
    'spinViewer/AssetsProvider',
    'spinViewer/SpinnerViewModel',
    'spinViewer/SpinnerController',
    'bindings/fadeVisible',
], function ($, viewHtmlString, Scene7config, Scene7ImageLoader, Scene7DataProvider, DataProvider, AssetsProvider, ViewModel, Controller) {
    'use strict';
    
    return {
        viewModel: {
            createViewModel : function (params, componentInfo) {
                var viewModelEvents = $({});
                var controllerEvents = $({});
                var containers = {
                    window: window,
                    root: componentInfo.element,
                    spin: componentInfo.element.querySelector('.amp-spin'),
                    images: componentInfo.element.querySelector('.amp-spin .amp-images')
                };
                var scene7config = new Scene7config(params.scene7.imageServer, params.scene7.videoServer);
                var scene7dataProvider = new Scene7DataProvider(scene7config);
                var dataProvider = new DataProvider(scene7config, scene7dataProvider);
                var assetsProvider = new AssetsProvider(dataProvider);
                var imageLoader = new Scene7ImageLoader(scene7config, params.viewer.useCors);
                var viewModel = new ViewModel(viewModelEvents, params.messages);
                var controller = new Controller(assetsProvider, imageLoader, params.viewer, viewModel, containers, controllerEvents);
                var api = {
                    loadAssets: controller.loadAssets.bind(controller),
                    setAssets: controller.setAssets.bind(controller),
                    refresh: controller.refresh.bind(controller),
                    init: controller.init.bind(controller),
                    showFrame: controller.showFrame.bind(controller),
                    getCurrentFrame: controller.getCurrentFrame.bind(controller),
                    getCurrentZoomer: controller.getCurrentZoomer,
                    events: controller.events,
                    zoomIn: controller.zoomIn.bind(controller),
                    zoomOut: controller.zoomOut.bind(controller),
                    zoomReset: controller.zoomReset.bind(controller),
                    canZoomIn: controller.canZoomIn,
                    canZoomOut: controller.canZoomOut,
                    hasError: controller.hasError,
                    isLoading: controller.isLoading,
                    resize: controller.resize.bind(controller)
                };
                params.api(api);
                return viewModel;
            }
        },
        template: viewHtmlString,
        synchronous: true
    };
});
define('spinViewer/ViewModel',['ko'], function (ko) {
    function ViewModel(events, params) {
        this.events = events;
        this.params = params;
        this.containerSpinViewerApi = ko.observable();
        this.fullScreenSpinViewerApi = ko.observable();
        this.containerSpinViewerApiReady = ko.observable(false);
        this.fullScreenSpinViewerApiReady = ko.observable(false);
        this.spinViewerApiReady = ko.computed(function () {
            return this.containerSpinViewerApiReady() && this.fullScreenSpinViewerApiReady();
        }, this);
        this.isContainerSpinViewerVisible = ko.observable(false);        
        this.isFullScreenSpinViewerVisible = ko.observable(false);
        this.areControlsVisible = ko.observable(true);
        this.areZoomButtonsVisible = ko.observable(false);
        this.isCloseButtonVisible = ko.observable(false);
        this.isFullScreenButtonVisible = ko.observable(false);
        this.isZoomInButtonEnabled = ko.observable(false);
        this.isZoomOutButtonEnabled = ko.observable(false);
        this.isSpinnable = ko.observable(false);

        this.zoomInButtonClicked = function () {
            this.events.trigger('zoomInButtonClicked');
        }.bind(this);
        this.zoomOutButtonClicked = function () {
            this.events.trigger('zoomOutButtonClicked');
        }.bind(this);
        this.closeButtonClicked = function () {
            this.events.trigger('closeButtonClicked');
        }.bind(this);      
        this.fullScreenButtonClicked = function () {
            this.events.trigger('fullScreenButtonClicked');
        }.bind(this);      
    }
    
    return ViewModel;
});
define(
    'spinViewer/Controller',[
        "ko",
        "$",
        "utils/deviceHelper",
        "utils/fullScreenHelper"
    ], function (ko, $, deviceHelper, fullScreenHelper) {
        'use strict';
        
        var Controller = function (assetsProvider, params, viewModel, containers, events) {
            this.assetsProvider = assetsProvider;
            this.events = events;
            this.containers = containers;
            this.params = {
                showZoomButtons: false,
                showCloseButton: false
            };
            this.viewModel = viewModel;
            this.isInitialized = false;
            $.extend(true, this.params, params);
            this.areControlsVisible = ko.computed(this.areControlsVisible, this);
            this.areZoomButtonsVisible = ko.computed(this.areZoomButtonsVisible, this);
            this.isZoomInButtonEnabled = ko.computed(this.isZoomInButtonEnabled, this);
            this.isZoomOutButtonEnabled = ko.computed(this.isZoomOutButtonEnabled, this);
            this.setEventListeners();
            this.viewModel.areControlsVisible(this.areControlsVisible());
            this.viewModel.areZoomButtonsVisible(this.areZoomButtonsVisible());
            this.viewModel.isZoomInButtonEnabled(this.isZoomInButtonEnabled());
            this.viewModel.isZoomOutButtonEnabled(this.isZoomOutButtonEnabled());
            this.viewModel.isCloseButtonVisible(this.params.showCloseButton);
            this.viewModel.isFullScreenButtonVisible(this.params.fullScreen === 'enabled');       
        };
        
        Controller.prototype.init = function (force, initFrame) {
            if (this.params.fullScreen === 'forced') {
                this.viewModel.isFullScreenSpinViewerVisible(true);
                this.viewModel.fullScreenSpinViewerApi().init(initFrame, this.containers.root);
                fullScreenHelper.launchIntoFullscreen(document, this.containers.root, false);
            }
            else {
                this.viewModel.isContainerSpinViewerVisible(true);
                this.viewModel.containerSpinViewerApi().init(initFrame);
            }
            this.isInitialized = true;
        };    
        
        Controller.prototype.areZoomButtonsVisible = function () {
            return this.params.showZoomButtons && (this.viewModel.isZoomInButtonEnabled() || this.viewModel.isZoomOutButtonEnabled());
        };

        Controller.prototype.areControlsVisible = function () {
            return this.viewModel.areZoomButtonsVisible() || this.viewModel.isFullScreenButtonVisible() || this.viewModel.isCloseButtonVisible();
        };
        
        Controller.prototype.isZoomInButtonEnabled = function () {
            return (this.viewModel.isFullScreenSpinViewerVisible() && this.viewModel.fullScreenSpinViewerApi().canZoomIn()) ||
            (this.viewModel.isContainerSpinViewerVisible() && this.viewModel.containerSpinViewerApi().canZoomIn());
        };
            
        Controller.prototype.isZoomOutButtonEnabled = function () {
            return (this.viewModel.isFullScreenSpinViewerVisible() && this.viewModel.fullScreenSpinViewerApi().canZoomOut()) ||
            (this.viewModel.isContainerSpinViewerVisible() && this.viewModel.containerSpinViewerApi().canZoomOut());
        };
        
        Controller.prototype.onCloseButtonClicked = function () {
            var fullScreenApi = this.viewModel.fullScreenSpinViewerApi(),
                containerApi = this.viewModel.containerSpinViewerApi(),
                currentFrame;
            
            this.viewModel.isContainerSpinViewerVisible(false);            
            if (!this.viewModel.isFullScreenSpinViewerVisible()) {
                containerApi.zoomReset();
            }
            else {
                this.viewModel.isFullScreenSpinViewerVisible(false);
                fullScreenHelper.exitFullScreen(document, this.containers.root, false);
                fullScreenApi.zoomReset();
            }
            this.events.trigger('playerExitRequested');
        };

        Controller.prototype.onFullScreenButtonClicked = function () {
            var fullScreenApi = this.viewModel.fullScreenSpinViewerApi(),
                containerApi = this.viewModel.containerSpinViewerApi(),
                currentFrame;

            if (!this.viewModel.isFullScreenSpinViewerVisible()) {
                this.viewModel.isContainerSpinViewerVisible(false);
                this.viewModel.isFullScreenSpinViewerVisible(true);
                currentFrame = containerApi.getCurrentFrame();
                fullScreenApi.init({ rowIndex: currentFrame.rowIndex, frameIndex: currentFrame.frameIndex }, this.containers.root);
            }
            else {
                fullScreenHelper.exitFullScreen(document, this.containers.root, false);
                this.viewModel.isContainerSpinViewerVisible(true);
                this.viewModel.isFullScreenSpinViewerVisible(false);
                currentFrame = fullScreenApi.getCurrentFrame();
                containerApi.init({ rowIndex: currentFrame.rowIndex, frameIndex: currentFrame.frameIndex });
            }
        };
        
        Controller.prototype.zoomIn = function (force, positionInContainer, onComplete) {
            if (force || this.viewModel.isZoomInButtonEnabled()) {
                this.events.trigger('zoomedIn');
                if (this.viewModel.isFullScreenSpinViewerVisible()) {
                    this.viewModel.fullScreenSpinViewerApi().zoomIn(force, positionInContainer, onComplete);
                }
                if (this.viewModel.isContainerSpinViewerVisible()) {
                    this.viewModel.containerSpinViewerApi().zoomIn(force, positionInContainer, onComplete);
                }
            }
            else {
                if (typeof onComplete === 'function') {
                    onComplete();
                }
            }
        };
        
        Controller.prototype.zoomReset = function (onComplete) {
            if (this.viewModel.isFullScreenSpinViewerVisible()) {
                this.viewModel.fullScreenSpinViewerApi().zoomReset(onComplete);
            }
            if (this.viewModel.isContainerSpinViewerVisible()) {
                this.viewModel.containerSpinViewerApi().zoomReset(onComplete);
            }
        }

        Controller.prototype.zoomOut = function (positionInContainer, onComplete) {
            if (this.viewModel.isZoomOutButtonEnabled()) {
                if (this.viewModel.isFullScreenSpinViewerVisible()) {
                    this.viewModel.fullScreenSpinViewerApi().zoomOut(positionInContainer, onComplete);
                }
                if (this.viewModel.isContainerSpinViewerVisible()) {
                    this.viewModel.containerSpinViewerApi().zoomOut(positionInContainer, onComplete);
                }
            }
            else {
                if (typeof onComplete === 'function') {
                    onComplete();
                }
            }
        };
        
        Controller.prototype.getEventPositionRelativeToTarget = function (e) {
            var target = e.target || e.srcElement,
                targetPosition = target.getBoundingClientRect(),
                position = {
                    x: e.clientX - targetPosition.left,
                    y: e.clientY - targetPosition.top
                };
            return position;
        };
        
        Controller.prototype.resize = function () {
            if (this.viewModel.isContainerSpinViewerVisible()) {
                this.viewModel.containerSpinViewerApi().resize();
            }
            if (this.viewModel.isFullScreenSpinViewerVisible()) {
                this.viewModel.fullScreenSpinViewerApi().resize();
            }
        };
        
        Controller.prototype.onSpun = function () {
            this.events.trigger('spun');
        };

        Controller.prototype.setEventListeners = function () {
            this.viewModel.events.on('fullScreenButtonClicked', function () {
                this.onFullScreenButtonClicked();
            }.bind(this));
            
            this.viewModel.events.on('closeButtonClicked', function () {
                this.onCloseButtonClicked();
            }.bind(this));            

            $(document).keyup(function (e) {
                if (e.keyCode == 27 && this.viewModel.isFullScreenSpinViewerVisible()) {
                    this.onCloseButtonClicked();
                }
            }.bind(this));
            
            this.viewModel.events.on("zoomInButtonClicked", this.zoomIn.bind(this, false, undefined, undefined));
            this.viewModel.events.on("zoomOutButtonClicked", this.zoomOut.bind(this, undefined, undefined));
            
            this.viewModel.spinViewerApiReady.subscribe(function (isReady) {
                if (isReady) {
                    $(window).resize(this.resize.bind(this));
                    this.viewModel.containerSpinViewerApi().events.on('refreshButtonClicked', function (e, containerFrame) {
                        this.viewModel.containerSpinViewerApi().refresh();
                    }.bind(this));
                    this.viewModel.fullScreenSpinViewerApi().events.on('refreshButtonClicked', function (e, fullScreenFrame) {
                        this.viewModel.fullScreenSpinViewerApi().refresh();
                    }.bind(this));
                    
                    this.viewModel.fullScreenSpinViewerApi().events.on('dblclick', function (e, originalEvent) {
                        this.zoomIn(true, this.getEventPositionRelativeToTarget(originalEvent), undefined);
                    }.bind(this));
                    this.viewModel.containerSpinViewerApi().events.on('dblclick', function (e, originalEvent) {
                        this.zoomIn(true, this.getEventPositionRelativeToTarget(originalEvent), undefined);
                    }.bind(this));

                    this.isZoomInButtonEnabled.subscribe(function (isZoomInButtonEnabled) {
                        this.viewModel.isZoomInButtonEnabled(isZoomInButtonEnabled);
                    }.bind(this));

                    this.isZoomOutButtonEnabled.subscribe(function (isZoomOutButtonEnabled) {
                        this.viewModel.isZoomOutButtonEnabled(isZoomOutButtonEnabled);
                    }.bind(this));
                    
                    this.viewModel.containerSpinViewerApi().events.on('spun', this.onSpun.bind(this));
                    this.viewModel.fullScreenSpinViewerApi().events.on('spun', this.onSpun.bind(this));

                    this.events.trigger('apiReady');
                }
            }.bind(this));
            
            this.areControlsVisible.subscribe(function (areControlsVisible) {
                this.viewModel.areControlsVisible(areControlsVisible);
            }.bind(this));
            
            this.areZoomButtonsVisible.subscribe(function (areZoomButtonsVisible) {
                this.viewModel.areZoomButtonsVisible(areZoomButtonsVisible);
            }.bind(this));            

            this.viewModel.fullScreenSpinViewerApi.subscribe(function (api) {
                if (api) {
                    this.viewModel.fullScreenSpinViewerApiReady(true);
                }
            }.bind(this));
            
            this.viewModel.containerSpinViewerApi.subscribe(function (api) {
                if (api) {
                    this.viewModel.containerSpinViewerApiReady(true);
                }
            }.bind(this));

        };
        
        Controller.prototype.loadAssets = function (id, onSuccess, onError) {
            this.isInitialized = false;
            this.assetsProvider.getAssets(
                id,
                function (assets) {
                    if (assets.maxSize.width && assets.maxSize.height) {
                        this.setAssets(assets);
                        if (typeof onSuccess === "function") {
                            onSuccess(assets);
                        }
                    }
                    else {
                        if (typeof onError === "function") {
                            onError();
                        }
                    }
                }.bind(this), 
                onError
            );
        };
        
        Controller.prototype.setAssets = function (assets) {
            this.isInitialized = false;
            this.viewModel.containerSpinViewerApi().setAssets(assets);
            this.viewModel.fullScreenSpinViewerApi().setAssets(assets);
            this.viewModel.isSpinnable(assets.frameUrls.length > 1 || (assets.frameUrls.length === 1 && assets.frameUrls[0].length > 1))
        };
        

        return Controller;
    });
define('spinViewer/component',[
    'ko',
    '$',
    'spinViewerView',
    'utils/scene7.config',
    'utils/scene7.DataProvider',
    'spinViewer/spinnerComponent',
    'spinViewer/DataProvider',
    'spinViewer/AssetsProvider',
    'spinViewer/ViewModel',
    'spinViewer/Controller'
], function (ko, $, viewHtmlString, Scene7config, Scene7DataProvider, spinnerComponent, DataProvider, AssetsProvider, ViewModel, Controller) {
    'use strict';
    
    ko.components.register('asos-spinner', spinnerComponent);

    return {
        viewModel: {
            createViewModel : function (params, componentInfo) {
                var viewModelEvents = $({});
                var controllerEvents = $({});
                var containers = {
                    root: componentInfo.element
                };
                var scene7config = new Scene7config(params.scene7.imageServer, params.scene7.videoServer);
                var scene7dataProvider = new Scene7DataProvider(scene7config);
                var dataProvider = new DataProvider(scene7config, scene7dataProvider);
                var assetsProvider = new AssetsProvider(dataProvider);
                if (params.viewer.forceFullScreen) {
                    params.viewer.fullScreen = 'forced';
                }
                var viewModel = new ViewModel(viewModelEvents, params);
                var controller = new Controller(assetsProvider, params.viewer, viewModel, containers, controllerEvents);
                
                controller.events.on('apiReady', function () {
                    var api = {
                        setAssets: controller.setAssets.bind(controller),
                        loadAssets: controller.loadAssets.bind(controller),
                        init: controller.init.bind(controller),
                        zoomIn: controller.zoomIn.bind(controller),
                        zoomOut: controller.zoomOut.bind(controller),
                        zoomReset: controller.zoomReset.bind(controller),
                        events: controller.events
                    };
                    params.api(api);
                });
                
                return viewModel;
            }
        },
        template: viewHtmlString,
        synchronous: true
    };
});
define(
    'Asos.Utils.MediaPlayers',[
        "videoPlayer/component",
        "spinViewer/component",
        'utils/scene7.config',
        'utils/scene7.DataProvider',
        'spinViewer/DataProvider',
        'spinViewer/AssetsProvider'
    ],
function (videoPlayerComponent, spinViewerComponent, Scene7config, Scene7DataProvider, DataProvider, AssetsProvider) {
        'use strict';
        
        function MediaPlayers() {
            this.videoPlayer = videoPlayerComponent;
            this.spinViewer = spinViewerComponent;
        }
        
        MediaPlayers.prototype.getSpinViewerAssetsProvider = function (imageServer) {
            var scene7config = new Scene7config(imageServer),
                scene7dataProvider = new Scene7DataProvider(scene7config),
                dataProvider = new DataProvider(scene7config, scene7dataProvider);
            return new AssetsProvider(dataProvider);            
        }

        return new MediaPlayers();
    });